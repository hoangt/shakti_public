/* Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Module name: Arithmetic Functions.
author name: Rishi Naidu
Email id:    rishinaidu.rn@gmail.com
last update done: 26th September 2013 

Contains different arithmetic functions as per the instructions. (*noinline) is included before every function so that it creates different module at compilation

_word_flag =1 for word instructions in which the operation is carried out only on lower 32 bits
_imm_flag =1 for immediate instructions
_sub_flag =1 for subtract instructions
_rl_ra_flag=1 for shift right arithmetic and =0 for shift right logical
_immediate is the immediate value from instruction
*/


package arithmetic_functions;
`include "defined_parameters.bsv"

//************** ADD/SUB INSTRUCTIONS **************************//
(*noinline*)
function  Bit#(32) fn_addsub (Bit#(32) _in1 , Bit#(32) _in2 , Bit#(12) _immediate , Bit#(1) _sub_flag , Bit#(1) _imm_flag);
	
	Bit#(32) op1_w=_in1[31:0];
	Bit#(32) op2_w=0;

	Bit#(32) op1=_in1;
	Bit#(32) op2=0;
	
   		//ADD ADDI SUB
	if (_imm_flag==1) op2 = signExtend(_immediate);
	else if (_sub_flag==1) op2 = ~_in2 + 1;
	else op2  = _in2;

	return (op1 + op2);

endfunction

//************** SET LESS THAN SIGNED AND UNSIGNED **************************//

//Function create a module for comparator which is then used multiple times
//Reduces hardware
(*noinline*)
function Bool fn_comparator( Bit#(32) _op1, Bit#(32) _op2);
	return (_op1 <_op2);
endfunction

//SIGNED INSTRUCTIONS
(*noinline*)
function Bit#(32) fn_slt(Bit#(32)_in1, Bit#(32) _in2, Bit#(12)_immediate , Bit#(1) _imm_flag);
	//SLT,SLTI 
	Bit#(32) rs1 = _in1;
	Bit#(32) rs2 = (_imm_flag==1) ? signExtend(_immediate) : _in2;

	if (rs1[31]==1 && rs2[31]==0) return 1;
	else if ((~(rs1[31] ^ rs2[31])==1) && fn_comparator(rs1,rs2)) return 1;
	else return 0;
endfunction

//UNSIGNED INSTRUCTIONS
(*noinline*)
function Bit#(32) fn_sltu(Bit#(32)_in1, Bit#(32) _in2, Bit#(12)_immediate , Bit#(1) _imm_flag);
	//SLTU,SLTIU
	Bit#(32) rs1 = _in1;
	Bit#(32) rs2 = (_imm_flag==1) ? signExtend(_immediate) : _in2;

	if (fn_comparator(rs1,rs2)) return 1;
	else return 0;
endfunction

//************************* SHIFT LEFT LOGICAL**************************//

//Function create a module for shift left which is then used multiple times
//Reduces hardware

(*noinline*)
function Bit#(32) fn_shiftleft (Bit#(32) _input, Bit#(5) _shiftamt);
	return _input << _shiftamt;
endfunction


(*noinline*)
function Bit#(32) fn_sll( Bit#(32) _in1 , Bit#(32) _in2 , Bit#(5) _immediate , Bit#(1) _imm_flag);
	Bit#(5) shift_amt=0;
	
	if (_imm_flag==1) shift_amt= _immediate;
	else shift_amt = _in2[4:0];
	return (fn_shiftleft (_in1,shift_amt));
		
endfunction

//*****************SHIFT RIGHT LOGICAL/ARITHMETIC INSTRUCITONS ******************//


//Function create a module for shift right which is then used multiple times
//Reduces hardware
(*noinline*)
function Bit#(64) fn_shiftright (Bit#(64) _input , Bit#(5) _shiftamt);
	return _input >> _shiftamt;
endfunction

(*noinline*)//SRL/SRLI/SRLW/SRLIW/SRAI/SRA/SRAIW/SRAW
function Bit#(32) fn_sra_srl(Bit#(32) _in1,Bit#(32) _in2,Bit#(5) _immediate, Bit#(1) rl_ra_flag, Bit#(1) _imm_flag);
	Bit#(5) shift_amt=0;

	if (_imm_flag==1) shift_amt= _immediate;
	else shift_amt= _in2[4:0];
	
	if (_in1[31] ==0 || rl_ra_flag==0) return fn_shiftright({32'd0,_in1},shift_amt)[31:0];
	else return fn_shiftright({32'hFFFFFFFF,_in1},shift_amt)[31:0];
 
endfunction

endpackage

