/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Neel Gala
Email ID : neelgala@gmail.com
last updated : Feb 20th 2015

Description : 
TODO provide proper logic for cacheable,bufferable_ and supervisor outpu signal to the bus.

This module is used to integrate the cache and processor and interface with the AHB-Lite bus. 
Requests from the both the cache for memory access are read and dcache is given priority over icache 
when simultaneous request occur. The design of the cache and the rules in this file ensure that 
neither of the caches starve and each is served atleast alternatively. 

*/
package core_new;

    import riscv::*;
	  import defined_types::*;
    import DReg::*;
	  `include "defined_parameters.bsv"

    interface Ifc_core;
            /* Inputs from the main memory*/
      method Action _data_from_memory(Bit#(TMul#(8,`Word_size)) datain);   // gets input from the memory
      method Action _bus_error_from_memory(bit buserror);                                     // recieves any bus error generated during a write/read operation to memory
              /* Output to the main memory*/
      method Maybe#(Bit#(`Addr_width)) address_to_memory_;                                    // address to the memory.
      method Bit#(TMul#(8,`Word_size)) data_to_memory_;                    // data to be written to memory
      method Bit#(1) rd_wr_to_memory_;                                                        // read of write operation to the memory.
      method Bit#(2) transfer_size_to_memory_();                                               // the size of read/write requested by the processor. 0- byte. 1-halfword(16 bits) 2- word (32bits) 3-doubleword (64bits)
      method Bit#(3) burst_mode_to_memory();                                                  // follows the AHB lit protocol.
      method bit cacheable_(); // 0= non-cacheable 1=cacheable
      method bit bufferable_(); // 0=nonbufferable 1= bufferable
      method bit supervisor_(); // 0 usermode 1=supervisor mode;
      method    Action      sin(Bit#(1) in);
      method    Bit#(1)     sout();
      method Bool flush;
      // Simulation only //
      method bit instruction_data ;// 0 means instruction request 1 means data request.
    endinterface

    typedef enum {Handling_Dcache,Handling_Icache,Idle} Controller_State deriving (Bits, Eq, FShow);

    (*synthesize*)
    module mkcore(Ifc_core);

    Ifc_riscv core <-mkriscv();

    Wire#(Maybe#(Bit#(`Addr_width))) wr_address_to_memory <-mkDWire(tagged Invalid);
    Wire#(Bit#(TMul#(8,`Word_size))) wr_data_to_memory <- mkDWire(0);
    Wire#(Bit#(1)) wr_rd_wr_to_memory <-mkDWire(0);
    Wire#(Bit#(2)) wr_transfer_size_to_memory <-mkDWire(0);
    Wire#(Bit#(3)) wr_burst_mode_to_memory <-mkDWire(0);
    Wire#(bit) wr_cacheable_to_memory<-mkDWire(0);   // TODO this will be generated by the cache; Now defaulting to non-cacheable
    Wire#(bit) wr_bufferable_to_memory<-mkDWire(0);  // TODO this will be generated by the cache; Now defaulting to non-bufferable
    Wire#(bit) wr_supervisor_to_memory<-mkDWire(1);  // TODO this should come from the register specifying the user mode. Now defaulting to supervisor mode as of now.

    Reg#(Maybe#(Bit#(TMul#(8,`Word_size)))) wr_data_from_memory<-mkDReg( tagged Invalid);
    Reg#(Bit#(1)) wr_bus_error_from_memory <-mkDReg(0);

    Reg#(Controller_State) rg_state <- mkReg(Idle);

    // Simulation purpose only //
    Wire#(bit) wr_instruction_data <-mkDWire(0);
    Wire#(Bool) wr_flush_everything<-mkDWire(False);

    rule read_flush_signal_from_cpu;
        wr_flush_everything<=core.flush_from_cpu_();
    endrule
    
    rule flush_caches(wr_flush_everything);
        rg_state<=Idle;
    endrule

    
    rule check_request_to_memory_from_either_ports(rg_state==Idle && !wr_flush_everything);
        if(core._address_data_out matches tagged Valid .addr_data)begin
            $display("CORE: Sending Dcache request to Memory");
            wr_address_to_memory<= tagged Valid addr_data;
            wr_transfer_size_to_memory<=core.byte_halfword_word_;
            wr_burst_mode_to_memory<=0;
            rg_state<=Handling_Dcache;
            wr_instruction_data<=1;
            if(core._data_out matches tagged Valid .data_out)begin //  write operation
              wr_data_to_memory<=data_out;
              wr_rd_wr_to_memory <=1;
            end
        end 
        else if(core._address_instruction_out matches tagged Valid .addr_data)begin
            $display("CORE: Sending Icache request to Memory");
            wr_address_to_memory<= tagged Valid addr_data;
            wr_transfer_size_to_memory<=2;
            wr_burst_mode_to_memory<=0;
            rg_state<=Handling_Icache;
            wr_instruction_data<=0;
        end
        else // if no request then stay in idle state.
            rg_state<=Idle;
    endrule
    rule send_response_from_memory_to_core(rg_state!=Idle && !wr_flush_everything); 
        if(wr_data_from_memory matches tagged Valid .data_mem)begin
            rg_state<=Idle;
            if(rg_state==Handling_Dcache)begin // dcache was being handled
                $display("Controller : Sending the data back to the DCACHE",$time);
                core._data_in(tagged Valid data_mem);
                core._data_bus_error(wr_bus_error_from_memory);
            end
            else if(rg_state==Handling_Icache)begin // if i-cache was being handled.
                $display("Controller : Sending the data back to the ICACHE",$time);
                core._instruction_in(tagged Valid data_mem);
                core._instruction_bus_error(wr_bus_error_from_memory);
            end
        end
    endrule
	
  method Maybe#(Bit#(`Addr_width)) address_to_memory_;                                    // address to the memory.
    return wr_address_to_memory;
  endmethod

	method Bit#(TMul#(8,`Word_size)) data_to_memory_;                    // data to be written to memory
        return wr_data_to_memory;
    endmethod

	method Bit#(1) rd_wr_to_memory_;                                                        // read of write operation to the memory.
        return wr_rd_wr_to_memory;
    endmethod

    method Bit#(2) transfer_size_to_memory_();                                               // the size of read/write requested by the processor. 0- byte. 1-halfword(16 bits) 2- word (32bits) 3-doubleword (64bits)
        return wr_transfer_size_to_memory;
    endmethod

    method Bit#(3) burst_mode_to_memory();                                                  // follows the AHB lit protocol.
        return wr_burst_mode_to_memory;
    endmethod

    method bit cacheable_(); // 0= non-cacheable 1=cacheable
        return wr_cacheable_to_memory;
    endmethod

    method bit bufferable_(); // 0=nonbufferable 1= bufferable
        return wr_bufferable_to_memory;
    endmethod

    method bit supervisor_(); // 0 usermode 1=supervisor mode;
        return wr_supervisor_to_memory;
    endmethod

    method Action _data_from_memory(Bit#(TMul#(8,`Word_size)) datain);   // gets input from the memory
        wr_data_from_memory<= tagged Valid datain;
    endmethod

	  method Action _bus_error_from_memory(bit buserror);                                     // recieves any bus error generated during a write/read operation to memory
        wr_bus_error_from_memory<=buserror;
    endmethod
    method bit instruction_data ; // 0 means instruction request 1 means data request.
            return wr_instruction_data;
    endmethod
    method    Action      sin(Bit#(1) in);
      core.sin(in);
    endmethod
    method    Bit#(1)     sout();
      return core.sout;
    endmethod
    method Bool flush;
      return wr_flush_everything;
    endmethod
    endmodule

endpackage
