
package Tb_core_new;
  import core_new::*;
	import RegFile::*;
	import defined_types::*;
	`include "defined_parameters.bsv"
    interface Ifc_Tbcore;
      method    Action      sin(Bit#(1) in);
      method    Bit#(1)     sout();
    endinterface
  
    (*synthesize*)
    module mkTb_core(Ifc_Tbcore);
      Ifc_core proc <-mkcore();


      RegFile#(Bit#(TSub#(`Addr_space,2)), Bit#(32)) dmem <-mkRegFileFullLoad("./code.hex");
//      RegFile#(Bit#(`Addr_space), Bit#(32)) imem <-mkRegFileFullLoad("./code.hex");
      Reg#(Maybe#(Bit#(32))) rg_data <-mkReg(tagged Invalid );
      Reg#(Bit#(2)) rg_count <-mkReg(0);
      Reg#(Bit#(32)) rg_clock<-mkReg(0);

      rule rl_clock;
        rg_clock<=rg_clock + 1;
      endrule
  
 //     rule rl_end(rg_clock==600);
 //       $finish(0);
 //     endrule
  
      rule read_request_from_proc(rg_data matches tagged Invalid);
          Bit#(32) data0=0;
          Bit#(32) data1=0;
          Bit#(32) data2=0;
          Bit#(32) data3=0;
          // all request from the instruction memory will be burst modes for simulation purpose. This needs to change later when integrating SOC.
/*          if(proc.address_to_memory_ matches tagged Valid .addr &&& proc.instruction_data==0) begin // imem request made read operation
              data0=imem.sub(addr[`Addr_space-1:2]);
              $display($time," Main Mem : Received request from I-cache for address : %h. Sending data : %h",addr,data0);
              rg_data<=tagged Valid (data0);
          end
          else */
         if(proc.address_to_memory_ matches tagged Valid .addr /*&&& proc.instruction_data==1*/ &&& proc.rd_wr_to_memory_==0)begin // dmem read request.
            data0=dmem.sub(addr[`Addr_space-1:2]);
            if(proc.transfer_size_to_memory_=='d2) // word transfer
              rg_data<= tagged Valid (zeroExtend(data0));
            else if (proc.transfer_size_to_memory_=='d1)begin // half_word
              if(addr[1:0] ==0)
                rg_data<= tagged Valid (zeroExtend(data0[15:0])); 
              else if(addr[1:0] ==2)
                rg_data<= tagged Valid (zeroExtend(data0[31:16])); 
            end
            else if (proc.transfer_size_to_memory_=='d0) begin// one byte
              if(addr[1:0] ==0)
                rg_data<= tagged Valid (zeroExtend(data0[7:0])); 
              else if(addr[1:0] ==1)
                rg_data<= tagged Valid (zeroExtend(data0[15:8])); 
              else if(addr[1:0] ==2)
                rg_data<= tagged Valid (zeroExtend(data0[23:16])); 
              else if(addr[1:0] ==3)
                rg_data<= tagged Valid (zeroExtend(data0[31:24])); 
            end
            $display(" Main Mem : Received single transaction request from D-cache READ for address : %h Size : %d sending data : %h",addr,proc.transfer_size_to_memory_,data0,$time);
          end
          else if(proc.address_to_memory_ matches tagged Valid .addr &&& proc.instruction_data==1 &&& proc.rd_wr_to_memory_==1)begin // dmem write request.
            data0=dmem.sub(addr[`Addr_space-1:2]);
            Bit#(32) new_data=0;
            if(proc.transfer_size_to_memory_=='d2) // word transfer
                new_data=proc.data_to_memory_[31:0];
            else if (proc.transfer_size_to_memory_=='d1)begin // half_word
               if(addr[1:0] ==0)
                  new_data={data0[31:16],proc.data_to_memory_[15:0]}; 
               else if(addr[1:0] ==1)
                  new_data={data0[31:24],proc.data_to_memory_[15:0],data0[7:0]}; 
               else if(addr[1:0] ==2)
                  new_data={proc.data_to_memory_[15:0],data0[15:0]}; 
            end
            else if (proc.transfer_size_to_memory_=='d0)begin // one byte
               if(addr[1:0] ==0)
                  new_data={data0[31:8],proc.data_to_memory_[7:0]}; 
               else if(addr[1:0] ==1)
                  new_data={data0[31:16],proc.data_to_memory_[7:0],data0[7:0]}; 
               else if(addr[1:0] ==2)
                  new_data={data0[31:24],proc.data_to_memory_[7:0],data0[15:0]}; 
               else if(addr[1:0] ==3)
                  new_data={proc.data_to_memory_[7:0],data0[23:0]}; 
            end
            dmem.upd(addr[`Addr_space-1:2],new_data);
            rg_data<=tagged Valid zeroExtend(new_data);
            $display($time," Main Mem : Received request from D-cache Write for address : %h Size : %d sending data : %h",addr,proc.transfer_size_to_memory_,new_data);
          end
      endrule

      rule send_data_to_cpu(rg_data matches tagged Valid .data);
          rg_data<=tagged Invalid;
          proc._data_from_memory(data);
      endrule
      method    Action      sin(Bit#(1) in);
        proc.sin(in);
      endmethod
      method    Bit#(1)     sout();
        return proc.sout;
      endmethod
      
    endmodule
endpackage
