/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Module name: Riscv_arithmetic_unit.
author name: Rishi Naidu, Neel Gala
Email id:    rishinaidu.rn@gmail.com, neelgala@gmail.com

This module is the arithmetic execution unit for the RISCV ISA. It is a 64 bit implementation which is named as RV64.
The instruction with a "W" are RV64 instructions which ignore the upper 32 bits and operate on the lower 32 bits.
The arithmetic unit is implemented as a single case statement where the instruction bits define the various operations to be executed.

This module contains single cycle MUL instruction execution.

*/

package alu;

import arithmetic_functions::*;
import defined_types::*;
`include "defined_parameters.bsv"
	(*noinline*)
	function Alu_output fn_arithmetic(Bit#(32) _instruction, Bit#(`Reg_width) _operand1, Bit#(`Reg_width) _operand2);
			Bit#(32) alu_result=0;
			case (_instruction) matches
			32'd0: begin
				alu_result=0;
			end
			default:begin
				Bit#(`Reg_width) zero=0;
				//Single cycle execution of DIV and MUL instructions
    `ifdef simulate
				if (_instruction[25]==1 && _instruction[6:0]=='b0110011) begin //MUL/DIV Instructions
          Bit#(TMul#(2,`Reg_width)) op1=0;
          Bit#(TMul#(2,`Reg_width)) op2=0;
			    Bit#(32) quotient='hfeed;
			    Bit#(32) remainder='hbabe;
          bit lv_take_complement=0;
          Bit#(32) sop1=0;
          Bit#(32) sop2=0;
					case (_instruction[14:12])
						3'b000: begin // MUL instruction
                            op1=signExtend(_operand1);
                            op2=signExtend(_operand2);
						end
						3'b001: begin // MULH instruction
                            op1={zero,((_operand1[`Reg_width-1]==1) ? (~_operand1 + 1):_operand1)};
                            op2={zero,((_operand2[`Reg_width-1]==1) ? (~_operand2 + 1):_operand2)};
							lv_take_complement = ((_operand1[`Reg_width-1] ^ _operand2[`Reg_width-1])==1) ? 1:0 ;	
						end
						3'b010: begin // MULHSU instruction
                            op1={zero,((_operand1[`Reg_width-1]==1) ? (~_operand1 + 1):_operand1)};
                            op2={zero,_operand2};
							lv_take_complement = (_operand1[`Reg_width-1]==1) ? 1 : 0 ;	
						end
						3'b011: begin // MULHU instruction
                            op1=zeroExtend(_operand1);
                            op2=zeroExtend(_operand2);
						end
            3'b100: begin // DIV
              sop1=_operand1[31]==1?~_operand1+1:_operand1;
              sop2=_operand2[31]==1?~_operand2+1:_operand2;
              if(sop2!=0)begin
                quotient=(sop1/sop2);
							  lv_take_complement = ((_operand1[`Reg_width-1] ^ _operand2[`Reg_width-1])==1) ? 1:0 ;	
              end
              else
                quotient='1;
            end
            3'b110: begin // REM
              sop1=_operand1[31]==1?~_operand1+1:_operand1;
              sop2=_operand2[31]==1?~_operand2+1:_operand2;
              if(sop2!=0)
                remainder=(sop1%sop2);
              else
                remainder=_operand1;
              if(remainder[31]!=_operand1[31])
                lv_take_complement=1;
            end
            3'b101,3'b111: begin // DIVU or REMU
              if(_operand2!=0)begin
                quotient=(_operand1/_operand2);
                remainder=(_operand1%_operand2);
              end
              else begin
                quotient='1;
                remainder=_operand1;
              end
            end
          endcase
		      Bit#(TMul#(2,`Reg_width)) lv_result_complement = op1*op2;
          Bit#(TMul#(2,`Reg_width)) final_result= (lv_take_complement==1)? (~lv_result_complement+1) : lv_result_complement;
          if(_instruction[14]==0) // some multiplier operation
            alu_result=(_instruction[14:12]=='b000)?lv_result_complement[`Reg_width-1:0]:final_result[63:`Reg_width];
          else if(_instruction[13]==1)
            alu_result=(lv_take_complement==1)?~remainder+1:remainder;
         else begin
            alu_result=(lv_take_complement==1)?~quotient+1:quotient;
          end
				end
				else begin // Arithmetic/ logical Instructions (except mul/div)
     `endif
					if(_instruction[2]== 1'b1) begin
                        if (_instruction[5]==1)begin 
						    alu_result =(zeroExtend(_instruction[31:12])<<12);   //LUI
                            //$display("LUI OUTPUT is : %h",alu_result);
                        end
                        else
						    alu_result =((zeroExtend(_instruction[31:12])<<12)+_operand1);//AUIPC // here operand1 will contain the PC. 
					end
					else if(_instruction[6:2]=='b00101)begin // AUIPC
						alu_result =((zeroExtend(_instruction[26:7])<<12)+_operand1); // here operand1 will contain the PC. 
					end
					else begin
						case (_instruction[14:12]) matches	
						3'b000: begin //ADD/ADDI/SUB
							alu_result =(fn_addsub(_operand1, _operand2, _instruction[31:20], _instruction[30],~_instruction[5]));
									     //(_operand1,_operand2, immediate, Subtract Instrn flag, Immediate Instrn flag)		
						end
						3'b001: begin //SLL/SLLI
							alu_result =(fn_sll(_operand1, _operand2, _instruction[24:20], ~_instruction[5]));
									// (_operand1, _operand2 , immediate, Immediate flag)
						end
						
						3'b010: begin //SLT/SLTI
							alu_result =(fn_slt(_operand1,_operand2,_instruction[31:20],~_instruction[5]));
									// (_operand1, _operand2 , immediate,  Immediate flag)
						end

						3'b011: begin //SLTU/SLTIU
							alu_result =(fn_sltu(_operand1,_operand2,_instruction[31:20],~_instruction[5]));
									// (_operand1, _operand2 , immediate,  Immediate flag	)
						end

						3'b100: begin //XOR/XORI
							if (_instruction[5]==1) alu_result =(_operand1 ^ _operand2);
							else alu_result =(_operand1 ^ signExtend(_instruction[31:20]));
						end
					
						3'b101: begin //SRL/SRLI/SRAI/SRA/
							// Shift_rigfht_logical/arithmetic flag ==1 for arithmetic shift and 0 for logical shift
							alu_result =(fn_sra_srl(_operand1,_operand2,_instruction[24:20],_instruction[30], ~_instruction[5]));
						end
						
						3'b110: begin //OR/ORI
							if (_instruction[5]==1) alu_result =(_operand1 | _operand2);
							else alu_result =(_operand1 | signExtend(_instruction[31:20]));
						end
						
						3'b111: begin //AND/ANDI
							if (_instruction[5]==1) alu_result =(_operand1 & _operand2);
							else alu_result =(_operand1 & signExtend(_instruction[31:20]));
						end
//						default:  $display("Unknown Instruction");
						endcase
					end
  `ifdef simulate		end  `endif   //Uncomment when using the single cycle multiply/division instruction
			end
		endcase
	Bit#(5) destination_address = _instruction[11:7];		
			
	return Alu_output{aluresult:alu_result,destination:destination_address};
	endfunction
	
endpackage
