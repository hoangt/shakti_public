/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Neel Gala, Arjun Menon
Email ID : neelgala@gmail.com, c.arjunmenon@gmail.com

Description : 

This module holds the registerFile of 32 registers. The operand forwarding is also done in this module.
For operand forwarding we  give priority to the Execution unit> Memory> Write-Back> RegisterFile.
When the data is forwarded by any of the stages, the stage will also indicate whether the valid data for the respective
rd is available or not. Suppose a LOAD memory instruction is in the execute stage, then the forwarded data will indicate False
on the valid structure but the destination address will be forwarded. Thus, the decoded stage will realise that there is an 
instruction in the pipe which will update the respective rd, but does not have updated value yet.

*/

package registerfile;

	import defined_types::*;
	`include "defined_parameters.bsv"
	import ConfigReg::*;
  import privilege_mapping::*;
  import myRS232::*;
  import GetPut            ::*;
  import DReg::*;

	interface Ifc_registerfile;
		method Action _inputs_from_decode_stage(Bit#(5) rs1, Bit#(5) rs2, Bit#(5) rd,Bool firing, Bool auipc, Bit#(32) pc,Bool is_priv,Bit#(3) priv_funct,Bit#(12) priv_addr,Bit#(1) priv_immediate);	// recives the input from the decode stage.
		method Action _inputs_from_writeback_stage (Bit#(5) destination, Bit#(`Reg_width) destination_value, Bool firing); // recieves the input from the writeback stage.
		method Action _forwarding_from_execution (Operand_forwading_type data); // forwarded data from the execution stage
		method Action _forwarding_from_memory (Operand_forwading_type data);	// forwarded data from the memory stage
		method Maybe#(Output_for_operand_fetch) output_to_decode_stage(); // return operands to the decodestage.
    method    Action      sin(Bit#(1) in);
    method    Bit#(1)     sout();
    `ifdef simulate
        method Action _print_all_rf(Bit#(32) pc, Bit#(5) dest, Bit#(`Reg_width) dest_value);
    `endif
	endinterface

	(*synthesize*)
	module mkregisterfile(Ifc_registerfile);

		Wire#(Bit#(5)) wr_rs1_decodestage <-mkDWire(0); // holds the address for operand1
		Wire#(Bit#(5)) wr_rs2_decodestage <-mkDWire(0); // holds the address for operand2
		Wire#(Bit#(5)) wr_rd_decodestage <-mkDWire(0);  // holds the address for destination regsiter
		Wire#(Bool) wr_decode_firing <-mkDWire(False); // if true, indicates that the decode stage is firing;
    Wire#(Bool) wr_auipc <-mkDWire(False); // indeicates whether its an AUIPC instruction or not.
    Wire#(Bool) wr_is_priv <-mkDWire(False); // indeicates whether its an AUIPC instruction or not.
    Wire#(Bit#(1)) wr_priv_immediate <-mkDWire(0); // indeicates whether its an AUIPC instruction or not.
    Wire#(Bit#(3)) wr_priv_funct <-mkDWire(0); // indeicates whether its an AUIPC instruction or not.
    Wire#(Bit#(12)) wr_priv_addr <-mkDWire(0); // indeicates whether its an AUIPC instruction or not.
    Wire#(Bit#(32)) wr_program_counter <-mkDWire(0); // holds the incoming program_counter value

		Wire#(Bit#(5)) wr_destination_wbstage <-mkDWire(0); // holds the address where the write needs to be poerformed by the WB stage
		Wire#(Bit#(`Reg_width)) wr_value_wbstage <-mkDWire(0); // holds the value to be commited.
		Wire#(Bool) wr_wb_firing <-mkDWire(False); // if true, indicates that the WB stage is firing;
		
		Wire#(Maybe#(Output_for_operand_fetch)) wr_output_to_decode <-mkDWire(tagged Invalid); // carries the output to the decode stage

		Wire#(Operand_forwading_type) wr_forward_from_EXE <-mkDWire(Operand_forwading_type{data_forward:0,rd_forward:0,valid:False});// holds the forwarded data from the execution stage
		Wire#(Operand_forwading_type) wr_forward_from_MEM <-mkDWire(Operand_forwading_type{data_forward:0,rd_forward:0,valid:False});// holds the forwarded data from the memory stage
		Wire#(Operand_forwading_type) wr_forward_from_WB  <-mkDWire(Operand_forwading_type{data_forward:0,rd_forward:0,valid:False});// holds the forwarded data from the writeback stage.

    /////////////////////////////////////////////////////// UART related wires //////////////////////////////////////////////////
    Wire#(Bit#(8)) wr_send_to_uart <-mkWire();
    Wire#(Bit#(1)) wr_transmission_notfull <-mkDWire(0);
    Wire#(Bit#(1)) wr_reciever_ready <-mkDWire(0);
    Wire#(Bool) wr_rx_reading_done <-mkDWire(False);
    Wire#(Bit#(8)) wr_data_from_uart <-mkDWire('h41);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////// Integer Register File ////////////////////////////////////////////////////////
		ConfigReg#(Bit#(`Reg_width)) integer_rf[32]; // delcaring the integer register file.
		for (Integer j = 0; j < 32; j = j + 1)begin
      if(j==2)
        integer_rf [j] <- mkConfigReg('hfff);
      else
  			integer_rf [j] <- mkConfigReg(0);
		end
    /////////////////////////// Machine Mode Special Register ///////////////////////////////////////////////////////////////////
    ConfigReg#(Bit#(`Reg_width)) rg_mcpuid <-mkConfigReg('h1129);//RV32IMAFD
    ConfigReg#(Bit#(`Reg_width)) rg_mimpid <-mkConfigReg('h38000);// C class and anonymous source
    ConfigReg#(Bit#(9)) rg_tx <-mkConfigReg(0);
    ConfigReg#(Bit#(9)) rg_rx <-mkConfigReg(0);
    ConfigReg#(Bit#(`Reg_width)) rg_mhartid <-mkConfigReg(0); // for now no thread support
		ConfigReg#(Bit#(`Reg_width)) machine_rf[32]; // delcaring the integer register file.
		for (Integer j = 0; j < 32; j = j + 1)begin
      if(j==0)
        machine_rf[0]<-mkConfigReg('d4095);
      else
  			machine_rf [j] <- mkConfigReg(0);
		end
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     ////////////////////////////////////////////// for Spike based debuggin only //////////////////////////////////////////////
    `ifdef simulate
      Reg#(Bit#(1)) rg_cnt <-mkReg(0);
      let reg_dump <- mkReg(InvalidFile) ;
      rule open_file(rg_cnt==0);
          String reg_dumpFile = "rtl_register_dump.txt" ;
          File lfh <- $fopen( reg_dumpFile, "w" ) ;
          if ( lfh == InvalidFile )begin
              $display("cannot open %s", reg_dumpFile);
              $finish(0);
          end
          rg_cnt <= 1 ;
          reg_dump <= lfh ;
      endrule
    `endif
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// UART Debugger /////////////////////////////////
      UART#(4) uart <-mkUART(8); // charasize,Parity,Stop Bits,BaudRate
      rule connect_uart_and_core;
        uart.parity_sel(NONE);
        uart.stopbits_sel(STOP_1);
        uart.divider('d163);
      endrule
      rule send_transimission_data0;
        uart.rx.put(wr_send_to_uart);
      endrule
      rule uart_tx_connection;
        wr_transmission_notfull<=pack(uart.transmission_fifo_notfull);
      endrule
//      rule write_into_transmission_buffer;
//        let x<-uart.tx.get;
//        rg_rx[8:0]<={1'b1,x};
//      endrule
      rule uart_rx_receiver_ready_signal;
        wr_reciever_ready<=pack(uart.recieve_fifo_notempty);
      endrule
      rule uart_rx_connection;
        let x<-uart.tx.get;
        wr_data_from_uart<=x;
      endrule
      rule assign_to_register;
        rg_rx[8:0]<={wr_reciever_ready,wr_data_from_uart};
      endrule
      rule remove_recieved_fifo_data(wr_rx_reading_done);
        $display($time," Dequeing the RX FIFO since it read");
        uart.dequeue_rx_fifo();
      endrule
    ////////////////////////////////////////////////////////////////////////////////


		rule update_the_regfile;//(!wr_flush);
			if(wr_decode_firing) begin// decode stage is firing;
				Maybe#(Bit#(`Reg_width)) rs1=tagged Invalid;
				Maybe#(Bit#(`Reg_width)) rs2=tagged Invalid;
    		
        if(wr_auipc)
          rs1=tagged Valid wr_program_counter;
				else if(wr_forward_from_EXE.rd_forward==wr_rs1_decodestage && wr_rs1_decodestage!=0)begin // if execution stage instruction is going to update rd.
					if(wr_forward_from_EXE.valid) // and the data is available
						rs1= tagged Valid wr_forward_from_EXE.data_forward;
				end
				else if(wr_forward_from_MEM.rd_forward==wr_rs1_decodestage && wr_rs1_decodestage!=0)begin // if memory stage instruciton is going to update rd
					if(wr_forward_from_MEM.valid) // and the data is available
						rs1= tagged Valid wr_forward_from_MEM.data_forward;
				end
				else if(wr_forward_from_WB.rd_forward==wr_rs1_decodestage && wr_rs1_decodestage!=0)begin // if write-back stage instruciton is going to update rd
					if(wr_forward_from_WB.valid) // and the data is available
						rs1= tagged Valid wr_forward_from_WB.data_forward;
				end
				else begin// if the data is present in the regsiter file pick from there
					rs1= tagged Valid integer_rf[wr_rs1_decodestage];
				end


				// if-else struct to find a valid operand2
				if(wr_forward_from_EXE.rd_forward==wr_rs2_decodestage && wr_rs2_decodestage!=0)begin // if execution stage instruction is going to update rd.
					if(wr_forward_from_EXE.valid) // and the data is available
						rs2= tagged Valid wr_forward_from_EXE.data_forward;
				end
				else if(wr_forward_from_MEM.rd_forward==wr_rs2_decodestage && wr_rs2_decodestage!=0)begin // if memory stage instruciton is going to update rd
					if(wr_forward_from_MEM.valid) // and the data is available
						rs2= tagged Valid wr_forward_from_MEM.data_forward;
				end
				else if(wr_forward_from_WB.rd_forward==wr_rs2_decodestage && wr_rs2_decodestage!=0)begin // if write-back stage instruciton is going to update rd
					if(wr_forward_from_WB.valid) // and the data is available
						rs2= tagged Valid wr_forward_from_WB.data_forward;
				end
				else begin// if the data is present in the regsiter file pick from there
					rs2= tagged Valid integer_rf[wr_rs2_decodestage];
				end
        ////////////////////////////////// PRIVELEGE INSTRUCTION MANIPULATION ////////////////////////////////////////////////////
        if(wr_is_priv)begin
          $display($time," PRIVILEGE INSTRUCTION: funct:%d addr :%h immediate: %d",wr_priv_funct,wr_priv_addr,wr_priv_immediate);
          Bit#(32) csr_read_data=0;
          if(wr_priv_addr==`MCPUID)
            csr_read_data=rg_mcpuid;
          else if(wr_priv_addr==`MIMPID)
            csr_read_data=rg_mimpid;
          else if(wr_priv_addr==`MHARTID)
            csr_read_data=rg_mhartid;
          else if(wr_priv_addr==`UARTTX)
            csr_read_data=zeroExtend(rg_tx);
          else if(wr_priv_addr==`UARTRX)begin
            wr_rx_reading_done<=True;
            csr_read_data=zeroExtend(rg_rx);
          end
          else 
            csr_read_data=machine_rf[priv_map(wr_priv_addr)];
          $display("csr_read_data:%h",csr_read_data);
          Bit#(32) csr_write_data=0;
          if(wr_priv_immediate==1)begin // immediate
            if(wr_priv_funct==1) // CSRW
              csr_write_data=zeroExtend(wr_rs1_decodestage);
            else if(wr_priv_funct==2) // CSRS
              csr_write_data=zeroExtend(wr_rs1_decodestage)|csr_read_data;
            else if(wr_priv_funct==3) // CSRC
              csr_write_data=~zeroExtend(wr_rs1_decodestage)&csr_read_data;              

            if(wr_priv_addr==`UARTTX && wr_priv_funct==1)begin // only if CSRW is used to write into UART register.
              $display("Writing into TX REGISTER :%d",csr_write_data);
              rg_tx[8:0]<={wr_transmission_notfull,truncate(csr_write_data)};
              wr_send_to_uart<=truncate(csr_write_data);
            end
            else
              machine_rf[priv_map(wr_priv_addr)]<=csr_write_data;

            wr_output_to_decode<=tagged Valid(Output_for_operand_fetch{rs1:csr_read_data,rs2:0});
          end
          else if(rs1 matches tagged Valid .rs1data)begin
            if(wr_priv_funct==1)//CSRW
              csr_write_data=rs1data;
            else if(wr_priv_funct==2) //CSRS
              csr_write_data=rs1data|csr_read_data;
            else if(wr_priv_funct==3)//CSRC
              csr_write_data=~rs1data&csr_read_data;              

            if(wr_priv_addr==`UARTTX && wr_priv_funct==1)begin
              $display("Writing into TX REGISTER :%h RS1DATA: %h",csr_write_data,rs1data);
              rg_tx[8:0]<={wr_transmission_notfull,truncate(csr_write_data)};
              wr_send_to_uart<=truncate(csr_write_data);
            end
            else begin
              machine_rf[priv_map(wr_priv_addr)]<=csr_write_data;
            end

            wr_output_to_decode<=tagged Valid(Output_for_operand_fetch{rs1:csr_read_data,rs2:0});
          end
        end
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if(rs1 matches tagged Valid .rs1data &&& rs2 matches tagged Valid .rs2data)begin // both the operands are available.
					$display("RF: Reg1 :%d (%h) Reg2 :%d (%h) Rd :%d",wr_rs1_decodestage,rs1,wr_rs2_decodestage,rs2,wr_rd_decodestage,$time);
					wr_output_to_decode<=tagged Valid (Output_for_operand_fetch{rs1:rs1data,rs2:rs2data});
          rg_tx[8]<=pack(wr_transmission_notfull);
  			end
				else begin // atleast one of the operands is not available.
          rg_tx[8]<=pack(wr_transmission_notfull);
					$display("RF: Waiting for operands rs1 : %d rs2 :%d",wr_rs1_decodestage,wr_rs2_decodestage,$time);
				end
			end
      else
          rg_tx[8]<=pack(wr_transmission_notfull);
			if(wr_wb_firing)begin 
				$display("1: Writing into Register : %d Value : %h ",wr_destination_wbstage,wr_value_wbstage,$time);
        if(wr_destination_wbstage!=0)
  				integer_rf[wr_destination_wbstage]<=wr_value_wbstage;
			end
		endrule

		method Action _inputs_from_decode_stage(Bit#(5) rs1, Bit#(5) rs2, Bit#(5) rd,Bool firing, Bool auipc, Bit#(32) pc,Bool is_priv,Bit#(3) priv_funct,Bit#(12) priv_addr,Bit#(1) priv_immediate);	// recives the input from the decode stage.
			wr_rs1_decodestage<=rs1;
			wr_rs2_decodestage<=rs2;
			wr_rd_decodestage<=rd;
			wr_decode_firing<=firing;
      wr_auipc<=auipc;
      wr_program_counter<=pc;
      wr_is_priv<=is_priv;
      wr_priv_immediate <= priv_immediate;
      wr_priv_funct<=priv_funct;
      wr_priv_addr<=priv_addr;
		endmethod

		method Action _inputs_from_writeback_stage (Bit#(5) destination, Bit#(`Reg_width) destination_value, Bool firing);
			wr_destination_wbstage<=destination;
			wr_value_wbstage<=destination_value;
			wr_wb_firing<=firing;
			wr_forward_from_WB <= Operand_forwading_type {	data_forward	: destination_value,
															rd_forward 	:destination,
															valid: True};
		endmethod
		
		method Action _forwarding_from_execution (Operand_forwading_type data);
			wr_forward_from_EXE <= data;
		endmethod
		
		method Action _forwarding_from_memory (Operand_forwading_type data);
			wr_forward_from_MEM <= data;
		endmethod

		method Maybe#(Output_for_operand_fetch) output_to_decode_stage();
			return wr_output_to_decode;
		endmethod
    
    method    Action      sin(Bit#(1) in);
      uart.rs232.sin(in);
    endmethod
    method    Bit#(1)     sout();
      return uart.rs232.sout;
    endmethod

    `ifdef simulate
      method Action _print_all_rf(Bit#(32) pc,Bit#(5) dest, Bit#(`Reg_width) dest_value)if(rg_cnt==1);
          Bit#(64) pc_val=zeroExtend(pc);
          $fwrite(reg_dump,"PC=%h\n",pc_val);
          for(Bit#(6) i=1;i<32;i=i+1)begin
            Bit#(64) temp;
            if(i[4:0]==dest)
              temp=signExtend(dest_value);
            else
              temp=signExtend(integer_rf[i[4:0]]);
            $fwrite(reg_dump,"REG %d %h\n",i,temp);
          end
          $fwrite(reg_dump,"\n");
      endmethod
    `endif

	endmodule
endpackage
