/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Neel Gala, Arjun Menon
Email ID : neelgala@gmail.com, c.arjunmenon@gmail.com

Description : 
This is the Top level of the c_class processor. It containes rules for each stage. The description of each stage 
is given in the respective rules.
*/
package riscv;

`include "defined_parameters.bsv"
import defined_types::*;
import ConfigReg::*;
import decoder::*;
import registerfile::*;
import alu::*;
import branch::*;
import memory::*;
import defined_types::*;
import bpu_tournament::*;
import SpecialFIFOs::*;
import FIFO::*;
import FIFOF::*;
import DReg::*;

	interface Ifc_riscv;
        // Methods for data fetch and write from/to the external evironment
		method Action _data_in (Maybe#(Bit#(32)) _input_data);
		method Maybe#(Bit#(32)) _data_out();
		method Maybe#(Bit#(`Addr_width)) _address_data_out();
		method Bit#(2) byte_halfword_word_();
    method Action _data_bus_error(bit busserror); // if 1 indicates bus error
    method Action _mis_aligned_data(Bool err);
        // Methods for instruction fetch from the external environment 
		method Action _instruction_in (Maybe#(Bit#(32)) _input_instruction);
		method Maybe#(Bit#(`Addr_width)) _address_instruction_out();
    method Action _instruction_bus_error(bit busserror); // if 1 indicates bus error
    method Action _mis_aligned_instruction(Bool err);

    method Bool flush_from_cpu_();
    method    Action      sin(Bit#(1) in);
    method    Bit#(1)     sout();
	endinterface
	
	(*synthesize*)
	module mkriscv(Ifc_riscv);

		// declaring all the execution units here.
		Ifc_bpu_tournament bpu  <-	mkbpu_tournament(); // instantiating the branch predictor
		Ifc_registerfile rf 		<- 	mkregisterfile(); // instantiating the registerFile.
		
		Wire#(Maybe#(Bit#(32))) wr_address_data_out <-mkDWire(tagged Invalid);// wire carryin the address to the data memory.
		Wire#(Maybe#(Bit#(32))) wr_data_out <-mkDWire(tagged Invalid);	// wire carrying the data to the data memory
		Wire#(Maybe#(Bit#(32))) wr_address_instruction_out <-mkDWire(tagged Invalid); // wire carrying the address to the instruction memory
		Wire#(Bool) wr_flush_everything <-mkDWire(False);	// if true inidicates that the entire pipe needs to be flushed
        /* registers involving input instruction read */
		Reg#(Maybe#(Bit#(32))) rg_input_instruction <-mkDReg(tagged Invalid);	// wire carrying the data from the instruction memory
    Reg#(Bool) rg_mis_aligned_instruction <-mkDReg(False); // incoming misaligned exception for instruction when set true
    Reg#(bit) rg_instruction_bus_error<-mkDReg(0); //when bus error occurs on the instruction fetch

        /* registers involving input data read */
		Reg#(Maybe#(Bit#(32))) rg_input_data <-mkDReg(tagged Invalid);// wire carryin the incoming data from the data memory
    Reg#(bit) rg_data_bus_error<-mkDReg(0); //when bus error occurs on the data read/write
    Reg#(Bool) rg_mis_aligned_data <-mkDReg(False);        // incoming misaligned exception for data when set true 

		//Wires to perform operand forwarding
		Wire#(Operand_forwading_type) wr_forward_from_EXE <-mkDWire(Operand_forwading_type{data_forward:0,rd_forward:0,valid:False});// holds the forwarded data from the memory stage
		Wire#(Operand_forwading_type) wr_forward_from_MEM <-mkDWire(Operand_forwading_type{data_forward:0,rd_forward:0,valid:False});// holds the forwarded data from the memory stage
		Wire#(Operand_forwading_type) wr_forward_from_WB  <-mkDWire(Operand_forwading_type{data_forward:0,rd_forward:0,valid:False});// holds the forwarded data from the writeback stage.
		Wire#(Decoded_info_type) wr_decoded_data <-mkDWire(Decoded_info_type{inst_type:ALU,  // instantiate the wire with random non-harmful data
																			rs1:0,rs2:0,rd:0,instruction:'h0000000,auipc:False,
                                      is_priv:False, priv_addr:0,priv_immediate:0,priv_funct:0});

//		Reg#(Atomic_mode) rg_atomic_mode <-mkReg(AMO_LOAD); // register to hold which mode of the atomic operation is currently under progress
//		Reg#(Bit#(32)) rg_amo_load_data <- mkReg(0);		// regsiter to hold the loaded data from memory for atomic operation
//		Reg#(Bit#(32)) rg_amo_store_data <- mkReg(0);		// register to hold the value to be stored in the memory for an atomic operation
		

		Wire#(Bit#(`Addr_width)) wr_effective_address <-mkDWire(0); // captures the new pc when a branch is mispredicted.
		Wire#(Bit#(2)) wr_byte_halfword_word <-mkDWire(2);			// indicates the size of the store being performed. 00-Byte. 01-Halfword. 10 -Word

		// declaring pipeline FIFOs (ISB = Inter- Stage - Buffer
		FIFOF#(IF_ID_type) ff_if_id <-mkLFIFOF(); // instantiating ISB between fetch and decode
		FIFOF#(ID_IE_type) ff_id_ie <-mkLFIFOF(); // instantiating ISB between decode and exec
		FIFOF#(IE_IMEM_type) ff_ie_imem <-mkLFIFOF(); // instantiating ISB between exec and memory
		FIFOF#(IMEM_IWB_type) ff_imem_iwb <-mkLFIFOF(); // instantiating ISB between memory and write-back
        //////////////////////// for debuggin only /////////////////////////////////
        `ifdef simulate
        Reg#(Bit#(2)) rg_cnt <-mkReg(0);
        let mem_dump <- mkReg(InvalidFile) ;
        rule open_file(rg_cnt==0);
            String mem_dumpFile = "rtl_memory_access_log.txt" ;
            File lfh1 <- $fopen( mem_dumpFile, "w" ) ;
            if ( lfh1 == InvalidFile )begin
                $display("cannot open %s", mem_dumpFile);
                $finish(0);
            end
            rg_cnt <= 1 ;
            mem_dump <= lfh1 ;
        endrule

        rule first_line(rg_cnt==1);
            $fwrite(mem_dump,"Memloc \t Value \t PC\n");
            rg_cnt<=2;
        endrule
        `endif
        ////////////////////////////////////////////////////////////////////////////

		// the following rule with forward the required data/result from each stage and send to the RF where operand forwardignn is handled.
		rule rl_forwarding_data_to_decode(!wr_flush_everything);
			rf._forwarding_from_execution(wr_forward_from_EXE); // forwarding from execution unit
			rf._forwarding_from_memory(wr_forward_from_MEM); // forwarding from memory unit.
		endrule

        ///////////////////////////////////////////////////////////// The instruction Fetch stage ///////////////////////////////////////////
		// the following rule fetches an instruction from the icache and incrementes the program_counter by 1 on every fetch.
    rule check_why_if_is_not_firing;
      $display("Flush:%b IF_ID_NOTFULL:%b",wr_flush_everything,ff_if_id.notFull);
    endrule
		rule rl_instruction_fetch(!wr_flush_everything);
      $display("****************** FETCH STAGE **********************",$time);
			if(rg_input_instruction matches tagged Valid .datain)begin 						// if the memory has responded with the reuquired instruction
        /* Check to see is there has been any exceptions generated during fetch*/
        Exception exec=None;
        if(rg_mis_aligned_instruction)
          exec=Instruction_misaligned;
        else if (rg_instruction_bus_error==1)
          exec=Instruction_buserr;

				// fill the next pipe fifo with the fetched instruction and the prediction type from the branch predictor
				ff_if_id.enq(IF_ID_type{ program_counter : bpu.send_output_().prog_counter_,
							 instruction	 : datain[31:0],
							 prediction:bpu.send_output_().prediction_,
                             exception:exec});
        $display("Instruction Fetched=%h \t program counter=%h ", datain,bpu.send_output_().prog_counter_,fshow(bpu.send_output_.prediction_));
				wr_address_instruction_out<= tagged Valid (bpu.next_pc_()); 				// send the next program counter to the instruction memory
				bpu._deq_FIFO(); 															// let the bpu know that the program counter is used and generate a new one.
			end
			else begin
				$display("Sending PC : %h",bpu.send_output_().prog_counter_,$time);
				wr_address_instruction_out<= tagged Valid (bpu.send_output_().prog_counter_); // if memory has not responded with the relevant data then keep sending the address out.
			end
		endrule

		/////////////////////////////////////////////////////////////// The decode and operand fetch stages ////////////////////////////////

		// The following rule is used to send the decoded information to the RF for fetching the operands
		rule rl_decode_and_operand_fetch1(!wr_flush_everything);
			let decoded_data = fn_decoder(ff_if_id.first().instruction); // decode the data
			wr_decoded_data <= decoded_data; 							// save the decoded data in a wire
			rf._inputs_from_decode_stage(decoded_data.rs1, decoded_data.rs2, decoded_data.rd, True,decoded_data.auipc,ff_if_id.first().program_counter,decoded_data.is_priv,decoded_data.priv_funct,decoded_data.priv_addr,decoded_data.priv_immediate); // send the decoded data for operand fetch to RF.
		endrule
		
		// The following rule is used to read output from the RF module.
		rule rl_decode_and_operand_fetch2(!wr_flush_everything);
			let operands_fetched=rf.output_to_decode_stage();		// output from the registerFile module which takes care of operand_forwarding also
           	$display("****************** Decode STAGE **********************PC : %h",ff_if_id.first().program_counter,$time);
			$display(" Decoded Instruction : %h PC : %h as ",ff_if_id.first().instruction,ff_if_id.first().program_counter,fshow(wr_decoded_data.inst_type));
			if(wr_decoded_data.inst_type==ILLEGAL)begin // if instruction is ILLEGAL then halt and wait for all previous instrucitons to complete. For simulation purpose only.
                if(ff_if_id.first().exception==None) // TODO take care of SYSCALL and SBREAK also here.
            					ff_id_ie.enq(ID_IE_type{rs1				: 0,
											rs2				: 0,
											instruction		: wr_decoded_data.instruction,
											inst_type		: wr_decoded_data.inst_type,
											program_counter	: ff_if_id.first().program_counter,
										 	prediction : ff_if_id.first().prediction,
                      exception: Illegal_instruction});
				$display("ILLEGAL OPERATION");
			end
			else if(operands_fetched matches tagged Valid .opfetched)begin // RF returned with valid operands
					ff_id_ie.enq(ID_IE_type{rs1				: opfetched.rs1,
											rs2				: opfetched.rs2,
											instruction		: wr_decoded_data.instruction,
											inst_type		: wr_decoded_data.inst_type,
											program_counter	: ff_if_id.first().program_counter,
										  	prediction : ff_if_id.first().prediction,
                                            exception: ff_if_id.first().exception});
					ff_if_id.deq(); // release the previous interstage buffer
			end
		endrule
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		/////////////////////////////// Sending inputs to execution units ///////////////////////////////////////////////////////////////////
		rule rl_execute_unit;
            $display("****************** EXECUTION STAGE **********************",$time);
			if(ff_id_ie.first().inst_type==NOP)begin
				ff_id_ie.deq();
			end
      else if(ff_id_ie.first().inst_type==PRIVELEGED)begin
				ff_ie_imem.enq(IE_IMEM_type{bypass:True,	// bypass the memory stage
											exec_result:ff_id_ie.first().rs1,
											program_counter		: ff_id_ie.first().program_counter,  // send the pc as well in case of flush at WB stage
											memory_data:0,		// all random data from here.
											word_size:0,
											signextend:0,
											mem_type:LOAD,
											amofunct:SWAP,
											destination_register:ff_id_ie.first().instruction[11:7],
                      exception:ff_id_ie.first().exception});
				              ff_id_ie.deq();
				wr_forward_from_EXE <= Operand_forwading_type {	data_forward 	: ff_id_ie.first().rs1,
																rd_forward  	: ff_id_ie.first().instruction[11:7],
																valid			: True};
      end
			else if(ff_id_ie.first().inst_type==ALU)begin // if decoded instruction is an ALU operation
				$display("Executing Arithmetic Instruction :%h PC :%h ",ff_id_ie.first().instruction,ff_id_ie.first().program_counter);
				let data=fn_arithmetic(ff_id_ie.first().instruction,ff_id_ie.first().rs1,ff_id_ie.first().rs2); // perform the respective ALU operation and fill next FIFO.
                $display("Output from ALU : %h",data);
				// forward the data to the register file for operand forwarding.
				wr_forward_from_EXE <= Operand_forwading_type {	data_forward 	: data.aluresult,
																rd_forward  	: data.destination,
																valid			: True};
				// enqueue the data in the next FIFO.
				ff_ie_imem.enq(IE_IMEM_type{bypass:True,	// bypass the memory stage
											exec_result:data.aluresult,
											program_counter		: ff_id_ie.first().program_counter,  // send the pc as well in case of flush at WB stage
											memory_data:0,		// all random data from here.
											word_size:0,
											signextend:0,
											mem_type:LOAD,
											amofunct:SWAP,
											destination_register:data.destination,
                                            exception:ff_id_ie.first().exception});

				ff_id_ie.deq();		// dequeue the previous FIFO
			end
			else if(ff_id_ie.first().inst_type==BRANCH)begin // is the decoded instruction is Branch instruction.
				$display("Executing BRANCH Instruction :%h PC : %h",ff_id_ie.first().instruction,ff_id_ie.first().program_counter);
        $display("Op1: %h,Op2 :%h",ff_id_ie.first().rs1,ff_id_ie.first().rs2);
				let data=fn_branch(ff_id_ie.first().program_counter,ff_id_ie.first().instruction,ff_id_ie.first().rs1,ff_id_ie.first().rs2,ff_id_ie.first().prediction); // execute the instruction
        if(data.pred_result matches tagged Mispredicted .address)begin
          $display("Misprediction PC : %h New PC: %h",ff_id_ie.first().program_counter,address);
          wr_flush_everything<=True; 
          wr_effective_address<=address; // change the PC on flush
        end
				ff_id_ie.deq(); // release the previous FIFO
        // forward the executed data to the register file for operand forwarding.
        wr_forward_from_EXE <= Operand_forwading_type {	data_forward 	: data.branchresult,
                                rd_forward  	: data.destination,
                                valid			: True};
      // enqueue the data into the next FIFO
        ff_ie_imem.enq(IE_IMEM_type{ bypass: True, // bypass the memory stage
                      exec_result:data.branchresult,	
                      destination_register:data.destination,
                      memory_data:0,		// all random from here
                      word_size:0,
                      signextend:0,
                      mem_type:LOAD,
                      amofunct:SWAP,
                      program_counter		: ff_id_ie.first().program_counter,
                      exception:ff_id_ie.first().exception});
				// send the prediction result to the branch prediction unit for training purpose.
				bpu._training(data.training_data.pc, data.training_data.branch_address, data.training_data.actual);
			end
			else if(ff_id_ie.first().inst_type==MEM || ff_id_ie.first().inst_type==AMO)begin // if the decoded instruction is an atomic or memory operation.
				let data = fn_memory(ff_id_ie.first().instruction,ff_id_ie.first().rs1,ff_id_ie.first().rs2); // calculate the respective address.
				// inform the register File that you have an instruction which will update the register rd but valid data is not available.
				$display("Executing MEM/ATOMIC Instruction :%h PC : %h. Addr : %h",ff_id_ie.first().instruction,ff_id_ie.first().program_counter,data.memory_address);
				wr_forward_from_EXE <= Operand_forwading_type {	data_forward 	: 0,
																rd_forward  	: data.destination,
																valid			: False}; // informs that the data is not yet available for forwarding.
				ff_ie_imem.enq(IE_IMEM_type{ bypass: False,						// cannot bypass the Memory stage
											exec_result:data.memory_address,	// memory address for load/store
											memory_data:data.memory_data,		// memory data for store.
											word_size:data.word_size,			// size of the data transfer
											signextend:data.signextend,			// whether the loaded data needs to be signextended before commiting
											mem_type:data.mem_type,				// AMO, STORE or LOAD
											destination_register:data.destination,	// destination where the LOADED value will be commited
											amofunct:data.amofunct,					// type of Atomic function.
											program_counter		: ff_id_ie.first().program_counter,
                                            exception:ff_id_ie.first().exception});
				ff_id_ie.deq();// release previous FIFO
			end
      else begin // some exception case
				ff_ie_imem.enq(IE_IMEM_type{bypass:True,	// bypass the memory stage
											exec_result:0,
											program_counter	: ff_id_ie.first().program_counter,  // send the pc as well in case of flush at WB stage
											memory_data:0,		// all random data from here.
											word_size:0,
											signextend:0,
											mem_type:LOAD,
											amofunct:SWAP,
											destination_register:0,
                      exception:ff_id_ie.first().exception});
      end
		endrule
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////// Memory Stage ///////////////////////////////////////////////////////////////////////////////

		rule rl_memory_stage
      `ifdef simulate 
        (rg_cnt==2)
      `endif
      ;
			let data=ff_ie_imem.first();
            $display("****************** MEMORY STAGE ********************** PC :%h",data.program_counter,$time);
			if(data.bypass==True)begin	// bypass the memory stage incase of ALU or branch instruction.
				$display("Bypassing the Memory stage for PC : %h",ff_ie_imem.first().program_counter);
				// forward the executed results to the regsiter file for operand forwarding.

				wr_forward_from_MEM <= Operand_forwading_type {	data_forward 	: data.exec_result,
																rd_forward   	: data.destination_register,
																valid : True};
				// enqueue data into the next FIFO
				ff_imem_iwb.enq(IMEM_IWB_type{	destination_address	: data.destination_register,
												destination_value	: data.exec_result,
												program_counter		: data.program_counter,
                                                exception:data.exception});
				ff_ie_imem.deq(); // release previous FIFO
			end
			else begin
/*				if(data.mem_type==ATOMIC)begin // if memory operation is an ATOMIC operation.
					Bit#(32) data_to_be_commited= 32'haaaaaaaa;
					if(rg_atomic_mode==AMO_LOAD)begin // first load the data from the memory. Default stage.
						$display("Atomic load Address : %h",data.exec_result);
						if(rg_input_data matches tagged Valid .datain)begin // when memory has returned with data
							$display("Memory responded with data : %h",datain);
							// TODO : AMOMIN and AMOMAX
							// TODO : This load needs to be reflected in the regsiter File.
							// TODO : Also what if there is an interrupt/branch in register file while atomic is in Memory stage. Half the excution might get over 
							//			leaving the processor in an unknown state.
							wr_address_data_out<= tagged Invalid;
							rg_amo_load_data<=datain; // store the loaded data in temp register to be commited.
							rg_atomic_mode<=AMO_STORE;	// switch to Store operation of the ATOMIC instruction.
							if(data.amofunct==SWAP )begin// AMOSWAP
								rg_amo_store_data<=data.memory_data;
							end
							else if(data.amofunct==ADD)begin// AMOADD
								rg_amo_store_data<=datain+data.memory_data;
							end
							else if(data.amofunct==XOR)begin// AMOXOR
								rg_amo_store_data<=datain^data.memory_data;
							end
							else if(data.amofunct==AND)begin// AMOAND
								rg_amo_store_data<=datain&data.memory_data;
							end
							else if(data.amofunct==OR)begin// AMOOR
								rg_amo_store_data<=datain|data.memory_data;
							end
							else if(data.amofunct==MINU)begin// AMOMINU
								if(datain<data.memory_data)
									rg_amo_store_data<=datain;
								else
									rg_amo_store_data<=data.memory_data;
							end
							else if(data.amofunct==MAXU)begin// AMOMAXU
								if(datain>data.memory_data)
									rg_amo_store_data<=datain;
								else
									rg_amo_store_data<=data.memory_data;
							end
							// forward the loaded data to the registerFile
							wr_forward_from_MEM <=Operand_forwading_type {	data_forward 	: datain,
																		rd_forward  	: data.destination_register,
																		valid: True};
						end
						else begin // send load address to the memory
							// indicate to registerFile that the current instruction will update rd but has not got the updated value yet.
							wr_forward_from_MEM <=Operand_forwading_type {	data_forward : 0,
																		rd_forward  	 : data.destination_register,
																		valid: False};
							wr_address_data_out<=tagged Valid data.exec_result(); // keep sendig the same address.
						end
					end
					else if(rg_atomic_mode==AMO_STORE)begin // performing the STORE part of the ATOMIC instruction.
						wr_forward_from_MEM <=Operand_forwading_type {	data_forward 	: rg_amo_load_data,
																		rd_forward  	: data.destination_register,
																		valid: True};// keep forwarding the loaded data to the registerFile.
						wr_data_out<= tagged Valid rg_amo_store_data; // keep sending the data to be written to output.

						$display("Atomic Store. Address : %h Data : %h",data.exec_result,rg_amo_store_data);
						if(rg_input_data matches tagged Valid .x)begin // if the store is successfully over by the memory continue
							$display("Atomic Store Over for PC : %h",data.program_counter);
							wr_address_data_out<= tagged Invalid; // release output address
							rg_atomic_mode<=AMO_LOAD; // set back to default LOAD for ATOMIC operation.
							// enqueue data into the next FIFO
							ff_imem_iwb.enq(IMEM_IWB_type{	destination_address	: data.destination_register,
															destination_value	: rg_amo_load_data,
															program_counter		: data.program_counter});
							ff_ie_imem.deq(); // release the previous FIFO
						end
						else
							wr_address_data_out<=tagged Valid data.exec_result; // keep sending the address to the output memory
					end

				end
				else */
            if(data.mem_type==LOAD)begin // if the executed instruction is a LOAD instruction
				$display("Normal Load operation for Address : %h",data.exec_result);
				if(rg_input_data matches tagged Valid .datain)begin // if memory has responded with loaded data.
					$display("Memory responded with data : %h",datain,$time);
					Bit#(32) data_to_be_commited= 32'haaaaaaaa;
					wr_address_data_out<=tagged Invalid; // release the output address line.
					if(data.signextend=='b1 && data.word_size=='b00) begin // LBU // unsigned byte load
						data_to_be_commited=zeroExtend(datain[7:0]);
					end
					else if (data.signextend=='b1 && data.word_size=='b01) begin // LHU unsigned half-word load
						data_to_be_commited=zeroExtend(datain[15:0]);
					end
					else if (data.signextend==0 && data.word_size=='b00) begin // LB signed byte load
						data_to_be_commited=signExtend(datain[7:0]);
					end 
					else if (data.signextend=='b0 && data.word_size=='b01) begin // LH signed hlafword load
						data_to_be_commited=signExtend(datain[15:0]);
					end	 
					else if (data.word_size=='b10) // LWU/LW // word load
						data_to_be_commited=datain;
				          `ifdef simulate
                    $fwrite(mem_dump,"%h\t0x%h\tPC=%h\tLOAD \n",{12'd0,data.exec_result[19:0]},data_to_be_commited,data.program_counter);
                  `endif
                    // when memory has responded with data, send to register file for operand forwarding.				
					wr_forward_from_MEM <=Operand_forwading_type {	data_forward 	: data_to_be_commited,
																	rd_forward  	: data.destination_register,
																	valid: True};
                    Exception exec=data.exception;
                    if(data.exception==None)
                        if(rg_mis_aligned_data)
                            exec=Load_misaligned;
                        else if(rg_data_bus_error==1)
                            exec=Load_buserr;

					// enqueue into the next FIFO
					ff_imem_iwb.enq(IMEM_IWB_type{	destination_address	: data.destination_register,
													destination_value	: data_to_be_commited,
													program_counter		: data.program_counter,
                                                    exception           : exec});
					ff_ie_imem.deq(); // release the previous FIFO.
				end
				else begin
    				wr_address_data_out<= tagged Valid data.exec_result; // keep the address line active
            $display("Sending Load address output: %d",data.exec_result); 
            // indicate registerFile that you have not recieved the loaded data for the given register.
				    wr_byte_halfword_word<=data.word_size; // send the size of transfer. 00 byte, 01 halfword, 10 word.
  					wr_forward_from_MEM <=Operand_forwading_type {	data_forward 	: 0,
																	rd_forward  	: data.destination_register,
																	valid: False};
				end
			end
			else if(data.mem_type==STORE)begin // the executed instruction is STORE operation.
				$display("Normal Store operation for Address : %h Data :%h ",data.exec_result,data.memory_data);
				wr_data_out<= tagged Valid (zeroExtend(data.memory_data)); // send the data to the output
				wr_byte_halfword_word<=data.word_size; // send the size of transfer. 00 byte, 01 halfword, 10 word.

                // nothing to the forward to the register file.
				wr_forward_from_MEM <=Operand_forwading_type {	data_forward 	: 0,
																rd_forward 	: 0,
																valid: False};
				if(rg_input_data matches tagged Valid .x)begin // if memory has completed the respective store.
				          `ifdef simulate
                    if(data.word_size==0) // byte store
                      $fwrite(mem_dump,"%h\t0x%h\tPC=%h\tSTORE\n",{12'd0,data.exec_result[19:0]},{24'd0,data.memory_data[7:0]},data.program_counter);
                    else if(data.word_size==1) // halfword store
                        $fwrite(mem_dump,"%h\t0x%h\tPC=%h\tSTORE\n",{12'd0,data.exec_result[19:0]},{16'd0,data.memory_data[15:0]},data.program_counter);
                    else if(data.word_size==2) // word store
                        $fwrite(mem_dump,"%h\t0x%h\tPC=%h\tSTORE\n",{12'd0,data.exec_result[19:0]},data.memory_data,data.program_counter);
                  `endif
          					wr_address_data_out<=tagged Invalid;// release the adddress line

                    Exception exec=data.exception;
                    if(data.exception==None)
                        if(rg_mis_aligned_data)
                            exec=Store_misaligned;
                        else if(rg_data_bus_error==1)
                            exec=Store_buserr;

                    // nothing to commit in the WB stage. We still enqueue in case an interrupt occurs which will cause the PC to stored in Stack.
					ff_imem_iwb.enq(IMEM_IWB_type{	destination_address	: 0,
													destination_value	: 0,
													program_counter		: data.program_counter,
                                                    exception:exec});
					ff_ie_imem.deq();// release the previous FIFO
				end
                else
    				wr_address_data_out<=tagged Valid data.exec_result; // keep address line active.

			end
		end
		endrule

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////// WRITE BACK STAGE /////////////////////////////////////////////////////////////
		rule rl_write_back;
            $display("*****************WRITE BACK STAGE*************************\t PC : %h",ff_imem_iwb.first().program_counter);
            `ifdef simulate
              rf._print_all_rf(ff_imem_iwb.first().program_counter,ff_imem_iwb.first().destination_address,ff_imem_iwb.first().destination_value);
            `endif
            if(ff_imem_iwb.first().exception==None)begin
              $display("Dest : %d %h",ff_imem_iwb.first().destination_address,ff_imem_iwb.first().destination_value);
              rf._inputs_from_writeback_stage(ff_imem_iwb.first().destination_address,ff_imem_iwb.first().destination_value,True);	
      			  ff_imem_iwb.deq(); // release the previous FIFO
            end
            else if(ff_imem_iwb.first().exception==Illegal_instruction)begin // TODO in case of other exceptions do the needful
                $display("Illegal reached Write-back");
                $finish(0);
            end
		endrule
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		rule rl_flush_everything(wr_flush_everything);	// flush the pipe
			bpu._flush(wr_effective_address);
			ff_if_id.clear();
		endrule

		rule rl_clock;
			$display("\n\n");
		endrule

		//////////////////////////// definition of methods /////////////////////////////////////////////////////////////////////////////////
		method Action _data_in (Maybe#(Bit#(32)) _input_data);
			rg_input_data<=_input_data;
		endmethod
		
		method Maybe#(Bit#(32)) _data_out();
			return wr_data_out;
		endmethod
	
		method Maybe#(Bit#(`Addr_width)) _address_data_out();
			return wr_address_data_out;
		endmethod
		method Action _instruction_in (Maybe#(Bit#(32)) _input_instruction);
			rg_input_instruction<=_input_instruction;
		endmethod
		method Maybe#(Bit#(`Addr_width)) _address_instruction_out();
			return wr_address_instruction_out;
		endmethod
		method Bit#(2) byte_halfword_word_();
			return wr_byte_halfword_word;
		endmethod
        
    method Action _instruction_bus_error(bit busserror); // if 1 indicates bus error
        rg_instruction_bus_error<=busserror;
    endmethod
    method Action _data_bus_error(bit busserror); // if 1 indicates bus error
        rg_data_bus_error<=busserror;
    endmethod

    method Bool flush_from_cpu_();
        return wr_flush_everything;
    endmethod
    method Action _mis_aligned_instruction(Bool err);
        rg_mis_aligned_instruction <=err;
    endmethod
    method Action _mis_aligned_data(Bool err);
        rg_mis_aligned_data <=err;
    endmethod
    
    method    Action      sin(Bit#(1) in);
      rf.sin(in);
    endmethod
    method    Bit#(1)     sout();
      return rf.sout;
    endmethod
	endmodule
endpackage


