package bridge;

import Clocks::*;
import ConfigReg::*;
/*------------------------------------------------------------------------------------------------------------------*/

interface Ifc_bridge;

	method Action _reset_from_proc();

	method Action _address_control_from_AHB(Bit#(32) _bus_address,Bit#(1) _read_write,Bit#(3) _word_size,Bit#(3)_burst_type,Bit#(4)_prot_type,Bit#(2) _transfer_type,Bit#(1)_slave_select,Bit#(1) _master_lock,Bit#(1) _valid_adress);

	method Action _data_from_AHB(Bit#(32) master_data,Bit#(1) valid_data);

	method Action _data_from_APB(Bit#(32) _slave_data,Bit#(1) _valid_slave_data);	
	
	method Action _response_from_APB(Bit#(1) _PREADY,Bit#(1) _methodread);

	method Action _response_from_AHB(Bit#(1) _AHB_data_ready);

	method Bit#(32)bridge_HADDR_();
	
	method Bit#(32)bridge_HWDATA_();

	method Bit#(32)bridge_HRDATA_();
	
	method Bit#(3)bridge_HSIZE_();

	method Bit#(3)bridge_HBURST_();
	
	method Bit#(4)bridge_HPROT_();

	method Bit#(2)bridge_HTRANS_();

	method Bit#(1)bridge_HMASTLOCK_();
	
	method Bit#(1)bridge_HSELX_();
	
	method Bit#(1)bridge_HWRITE_();
	
	method Bit#(1)bridge_HREADY_();

	method Bit#(1)bridge_HRESP_();

	method Bit#(1) bridge_slave_valid_data();

	method Bit#(1) bridge_hrdata_ready();

	

endinterface

/*------------------------------------------------------------------------------------------------------------------*/
(*synthesize*)
module mkbridge(Clock clk2,Reset rst2,Ifc_bridge ifc);

	//OUTPUT SYNCFIFOS FOR ADDRESS , CONTROL & DATA TO APB

	Clock clk1  <-exposeCurrentClock;
	Reset rst1  <-exposeCurrentReset;

	SyncFIFOIfc #(Bit#(32)) fifo_HADDR      <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(1))  fifo_HWRITE     <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(3))  fifo_HSIZE      <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(3))  fifo_HBURST     <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(4))  fifo_HPROT      <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(2))  fifo_HTRANS     <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(1))  fifo_HSELX      <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(1))  fifo_HMASTLOCK  <- mkSyncFIFO(4,clk1,rst1,clk2);
	SyncFIFOIfc #(Bit#(32)) fifo_HWDATA     <- mkSyncFIFO(4,clk1,rst1,clk2);
	Reg #(Bit#(1))  rg_bridge_to_APB_read_data_ready  <-mkReg(1,clocked_by clk2,reset_by rst2);

	//INTERNAL REGISTERS AND WIRES.

	Wire #(Bool)	wr_proc_reset		       <-mkDWire(False);
	Reg #(Bit#(32)) rg_bridge_HADDR                <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(1))  rg_bridge_HWRITE               <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(3))  rg_bridge_HSIZE                <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(3))  rg_bridge_HBURST               <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(4))  rg_bridge_HPROT                <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(2))  rg_bridge_HTRANS               <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(1))  rg_bridge_HSELX                <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(1))  rg_bridge_HMASTLOCK            <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(32)) rg_bridge_HWDATA     	       <-mkConfigReg(0,clocked_by clk1,reset_by rst1);
	Wire#(Bit#(1))  wr_ahb_to_bridge_valid_write_data       <-mkDWire(0);
	Wire#(Bit#(1))  wr_ahb_to_bridge_valid_write_address    <-mkDWire(0);
	Reg #(Bit#(1))  rg_ahb_to_bridge_valid_write_address    <-mkReg(0);

	//INPUT FROM THE AHB.

	Wire #(Bit#(32)) wr_bridge_HADDR               <-mkDWire(0);
	Wire #(Bit#(1))  wr_bridge_HWRITE              <-mkDWire(0);
	Wire #(Bit#(3))  wr_bridge_HSIZE               <-mkDWire(0);
	Wire #(Bit#(3))  wr_bridge_HBURST              <-mkDWire(0);
	Wire #(Bit#(4))  wr_bridge_HPROT               <-mkDWire(0);
	Wire #(Bit#(2))  wr_bridge_HTRANS              <-mkDWire(0);
	Wire #(Bit#(1))  wr_bridge_HSELX               <-mkDWire(0);
	Wire #(Bit#(1))  wr_bridge_HMASTLOCK           <-mkDWire(0);	
	Wire #(Bit#(32)) wr_bridge_HWDATA              <-mkDWire(0);
	Wire #(Bit#(1))  wr_ahb_to_bridge_read_data_ready             <-mkDWire(0);

	//RESPONSE FROM APB
	Wire #(Bit#(1)) wr_bridge_deque                <-mkDWire(0,clocked_by clk2,reset_by rst2);
	Wire #(Bit#(1)) wr_bridge_PREADY	       <-mkDWire(0,clocked_by clk2,reset_by rst2);

	//OUTPUT SIGNALS TO AHB
	SyncFIFOIfc #(Bit#(32)) fifo_HRDATA            <- mkSyncFIFO(4,clk2,rst2,clk1);
	Reg #(Bit#(1))  rg_bridge_HREADY               <-mkReg(1,clocked_by clk1,reset_by rst1);
	Reg #(Bit#(1))  rg_bridge_HRESP                <-mkReg(1,clocked_by clk1,reset_by rst1);
	SyncFIFOIfc #(Bit#(1)) fifo_bridge_to_AHB_valid_data   <- mkSyncFIFO(4,clk2,rst2,clk1);

/*------------------------------------------------------------------------------------------------------------------*/

	rule rl_reset_from_proc(wr_proc_reset);
		
		/*fifo_HADDR.clear(rg_bridge_HADDR);
		fifo_HWRITE.clear(rg_bridge_HWRITE);
		fifo_HSIZE.clear(rg_bridge_HSIZE);
		fifo_HBURST.clear(rg_bridge_HBURST);
		fifo_HPROT.clear(rg_bridge_HPROT);
		fifo_HTRANS.clear(rg_bridge_HTRANS);
		fifo_HSELX.clear(rg_bridge_HSELX);
		fifo_HMASTLOCK.clear(rg_bridge_HMASTLOCK);
*/
		rg_bridge_HADDR      <=	0;
		rg_bridge_HWRITE     <=	0;
		rg_bridge_HSIZE      <=	0;
		rg_bridge_HBURST     <=	0;
		rg_bridge_HPROT      <=	0;
		rg_bridge_HTRANS     <=	0;
		rg_bridge_HSELX      <=	0;
		rg_bridge_HMASTLOCK  <=	0;
		rg_bridge_HREADY     <= 1;
		rg_bridge_HRESP      <= 1;
		//rg_bridge_PREADY     <=1;
	endrule
	
	rule rl_address_control_delay;
	   rg_bridge_HADDR        		<=	wr_bridge_HADDR;
	   rg_bridge_HWRITE       		<=	wr_bridge_HWRITE;
	   rg_bridge_HSIZE        		<=	wr_bridge_HSIZE;
	   rg_bridge_HBURST       		<=	wr_bridge_HBURST;
	   rg_bridge_HPROT        		<=	wr_bridge_HPROT;
	   rg_bridge_HTRANS       		<=	wr_bridge_HTRANS;
	   rg_bridge_HSELX        		<=	wr_bridge_HSELX;
	   rg_bridge_HMASTLOCK    		<=	wr_bridge_HMASTLOCK;
	   rg_ahb_to_bridge_valid_write_address		<=      wr_ahb_to_bridge_valid_write_address;
	endrule	   


	rule rl_enque_address_control(rg_ahb_to_bridge_valid_write_address==1);
		fifo_HADDR.enq(rg_bridge_HADDR);
		fifo_HWRITE.enq(rg_bridge_HWRITE);
		fifo_HSIZE.enq(rg_bridge_HSIZE);
		fifo_HBURST.enq(rg_bridge_HBURST);
		fifo_HPROT.enq(rg_bridge_HPROT);
		fifo_HTRANS.enq(rg_bridge_HTRANS);
		fifo_HSELX.enq(rg_bridge_HSELX);
		fifo_HMASTLOCK.enq(rg_bridge_HMASTLOCK);
		//$display("ENQ**********");
	endrule
	
	rule rl_enque_data_from_AHB;
	
		if(wr_ahb_to_bridge_valid_write_data==1)begin
			fifo_HWDATA.enq(wr_bridge_HWDATA);
			rg_bridge_HWDATA<=wr_bridge_HWDATA;
		end
	endrule


	rule rl_Hready_Hresp_to_the_AHB;
		if(fifo_HADDR.notFull() || fifo_HWDATA.notFull())begin
			rg_bridge_HREADY	<=1;
			rg_bridge_HRESP		<=1;
		end
		else begin 
			rg_bridge_HREADY	<=0;
			rg_bridge_HRESP		<=0;
		end
	endrule

	rule rl_deque_address_control_data_from_AHB;
	//$display("bridge_ready=%d,wr_bridge_deque=%d ",wr_bridge_PREADY,wr_bridge_deque);
	if(fifo_HADDR.notEmpty() && wr_bridge_PREADY==1 &&  wr_bridge_deque==1)begin
			fifo_HADDR.deq();
			fifo_HWRITE.deq();
			fifo_HSIZE.deq();
			fifo_HBURST.deq();
			fifo_HPROT.deq();
			fifo_HTRANS.deq();
			fifo_HSELX.deq();
			fifo_HMASTLOCK.deq();
			end
		if(fifo_HWRITE.first()==1 && fifo_HADDR.notEmpty() && wr_bridge_PREADY==1 && wr_bridge_deque==1)begin
			fifo_HWDATA.deq();
		end
	endrule		

	rule rl_deque_read_data_from_APB;

	if(wr_ahb_to_bridge_read_data_ready == 1)begin
		
		fifo_HRDATA.deq();
		fifo_bridge_to_AHB_valid_data.deq();
	end
	endrule

	rule rl_bridge_hrdata;
		if(fifo_HRDATA.notFull())
			rg_bridge_to_APB_read_data_ready<=1;
		else
			rg_bridge_to_APB_read_data_ready<=0;
	endrule

/*
rule rl_display1;
	$display("Bridge_before_enq >>>>>>HADDR=%d, HWRITE=%d, HSIZE=%d, HBURST=%d, HTRANS=%d, HWDATA=%d",rg_bridge_HADDR,rg_bridge_HWRITE,rg_bridge_HSIZE,rg_bridge_HBURST,rg_bridge_HTRANS,rg_bridge_HWDATA);
		$display("-----------------------------------------------------------------------------------------------------------");
	endrule
*/	
/*
rule rl_display2;
	$display("Bridge_clk2 >>>>>>HADDR=%d,HWDATA=%d",fifo_HADDR.first(),fifo_HWDATA.first());
endrule
*/

/*
rule rl_display3;
	$display("Bridge_after_enque >>>>>>HADDR=%d, HWRITE=%d, HSIZE=%d, HBURST=%d, HTRANS=%d, fifo.notempty()=%B",fifo_HADDR.first(),fifo_HWRITE.first(),fifo_HSIZE.first(),fifo_HBURST.first(),fifo_HTRANS.first(),fifo_HADDR.notEmpty());
	$display("first_method");
endrule
/*------------------------------------------------------------------------------------------------------------------*/

/* RESET FROM THE PROCESSOR*/

	method Action _reset_from_proc();
			wr_proc_reset <=True;
	endmethod	
	
/* ADDRESS & CONTROL SIGNALS FROM THE AHB */
	method Action _address_control_from_AHB(Bit#(32) _bus_address,Bit#(1) _read_write,Bit#(3) _word_size,Bit#(3)_burst_type,Bit#(4)_prot_type,Bit#(2) _transfer_type,Bit#(1)_slave_select,Bit#(1) _master_lock,Bit#(1) _valid_address);

	   wr_bridge_HADDR            	<=	_bus_address ;
	   wr_bridge_HWRITE           	<=	_read_write;
	   wr_bridge_HSIZE            	<=	_word_size;
	   wr_bridge_HBURST           	<=	_burst_type;
	   wr_bridge_HPROT            	<=	_prot_type;
	   wr_bridge_HTRANS           	<=	_transfer_type;
	   wr_bridge_HSELX            	<=	_slave_select;
	   wr_bridge_HMASTLOCK        	<=	_master_lock;
	   wr_ahb_to_bridge_valid_write_address	<=    _valid_address;
	  

	endmethod

/*FIFO_RESPONSE_FROM_AHB*/

method Action _response_from_AHB(Bit#(1) _AHB_data_ready);
	wr_ahb_to_bridge_read_data_ready <= _AHB_data_ready;
endmethod


/* DATA FROM AHB */
	method Action _data_from_AHB(Bit#(32) master_data,Bit#(1) valid_data);
		wr_ahb_to_bridge_valid_write_data   <=valid_data;
		wr_bridge_HWDATA     	   <=master_data;
	   
	endmethod

/* DATA FROM APB*/
	method Action _data_from_APB(Bit#(32)_slave_data,Bit#(1) _valid_slave_data);
		if(_valid_slave_data==1 && fifo_HRDATA.notFull())begin
	   fifo_HRDATA.enq(_slave_data);
	   fifo_bridge_to_AHB_valid_data.enq(_valid_slave_data);
		end
	endmethod
	
/*REPONSE SIGNALS FROM APB */
	method Action _response_from_APB(Bit#(1) _PREADY,Bit#(1) _bridge_deque);
		wr_bridge_PREADY      <= _PREADY;
		wr_bridge_deque    <= _bridge_deque;
		//$display("BRIDGE >>>>>>> bridge_deque==%d",_bridge_deque);
	endmethod
	
	method Bit#(32)bridge_HADDR_();
		return fifo_HADDR.first();
	endmethod
	
	method Bit#(32)bridge_HWDATA_();
		return fifo_HWDATA.first();
	endmethod

	method Bit#(32)bridge_HRDATA_();
		return fifo_HRDATA.first();
	endmethod

	method Bit#(3)bridge_HSIZE_();
		return fifo_HSIZE.first();
	endmethod

	method Bit#(3)bridge_HBURST_();
		return fifo_HBURST.first();
	endmethod

	method Bit#(4)bridge_HPROT_();
		return fifo_HPROT.first();
	endmethod

	method Bit#(2)bridge_HTRANS_();
		return fifo_HTRANS.first();
	endmethod

	method Bit#(1)bridge_HMASTLOCK_();
		return fifo_HMASTLOCK.first();
	endmethod

	method Bit#(1)bridge_HSELX_();
		return fifo_HSELX.first();
	endmethod	

	method Bit#(1)bridge_HWRITE_();
		return fifo_HWRITE.first();
	endmethod 

	method Bit#(1) bridge_HREADY_();
		return rg_bridge_HREADY;
	endmethod

	method Bit#(1) bridge_HRESP_();
		return rg_bridge_HRESP;
	endmethod

	method Bit#(1) bridge_slave_valid_data();
		return fifo_bridge_to_AHB_valid_data.first();
	endmethod
	  
	method Bit#(1) bridge_hrdata_ready();
		return rg_bridge_to_APB_read_data_ready;
	endmethod
endmodule

endpackage
