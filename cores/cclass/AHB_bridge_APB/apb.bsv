/*
module name: APB
authors: Vighnesh Kamath, Sushmitha Ayyanar, Narthanaa Krishnan
email id: vighnesh.kamath200190@gmail.com
email id: sushmithauks11@gmail.com
last update done on 1st July, 2014.

This is an APB module. The protocol's state diagram has 3 states, namely - idle, setup and enable.
*/

package apb;                                                                                                                                                                                          

import ConfigReg::*;
interface Ifc_apb;

method Action _address_and_control_from_bridge(Bit#(32) _address,Bit#(1) _write,Bit#(3) _data_size, Bit#(3) _burst_type, Bit#(4) _prot_type,Bit #(2) _transfer_type, Bit #(1) _bridge_lock, Bit#(1) _Hselect);

method Action _reset_from_proc(Bit#(1) _proc_reset);

method Action _data_from_slave(Bit#(32) _slave_data);

method Action _data_from_bridge(Bit#(32) _bridge_data);

method Action _response_from_slave(Bit#(1) _slave_ready,Bit#(1) _slave_error);

method Action _busy_from_bridge(Bit#(1) _bridge_fifo_ready);

method Bit#(1) pBUSY_();

method Bit#(1) pSELX_();

method Bit#(1) pENABLE_();

method Bit#(1) pWRITE_();

method Bit#(32) pADDR_();

method Bit#(1) pREADY_();

method Bit#(32) pRDATA_();

method Bit#(32) pWDATA_();

method Bit#(1) pSLVERR_();

method Bit#(1) bridge_deque_();

method Bit#(1) valid_slave_data_();

endinterface

(*synthesize*)
      
module mkapb(Ifc_apb);

//STATE MACHINE VARIABLE.

	Reg #(Bit#(2))rg_state           <-mkConfigReg(0);	//state of the protocol 

//INTERNAL WIRES & REGISTERS.

	Wire #(Bool)wr_input_fire  	   <-mkDWire(False);	
	Reg#(Bit#(1))rg_bridge_deque    <- mkReg(0);//for dequeing the bridge contents.
	
//RESET SIGNAL FROM THE PROCESSOR

	Wire #(Bit#(1)) wr_proc_reset  <-mkDWire(0);	//reset from the processor - active high

//INPUT ADDRESS,CONTROL & DATA SIGNALS FROM THE MASTER.

	Wire #(Bit#(32)) wr_HADDR     <-mkDWire(0);	//address from AHB
	Wire #(Bit#(1))  wr_HWRITE    <-mkDWire(0);	//if hwrite=1 it implies that data is written in slave, and vice versa
	Wire #(Bit#(3))  wr_HSIZE     <-mkDWire(0);	//the size of a data
	Wire #(Bit#(3))  wr_HBURST    <-mkDWire(0);	//gives the type of the burst mode
	Wire #(Bit#(4))  wr_HPROT     <-mkDWire(0);	//*
	Wire #(Bit#(2))  wr_HTRANS    <-mkDWire(0);	//gives the type of the transfer
	Wire #(Bit#(1))  wr_HMASTLOCK <-mkDWire(0);	//*
	Wire #(Bit#(1))  wr_HSELX     <-mkDWire(0);	//used to select one particular slave from a number of slaves
	Wire #(Bit#(32)) wr_HWDATA    <-mkDWire(0);	//data to be written into the slave
	Wire #(Bit#(1))  wr_proc_HRDATA_read_ready     <-mkDWire(0);//stall the slave in case of bridge busy
//OUTPUT RESPONSE TO THE BRIDGE.

	Reg #(Bit#(1)) rg_PREADY      <- mkConfigReg(1);	//signal by slave to indicate to p
	Reg #(Bit#(1)) rg_PSLVERR     <- mkConfigReg(1);

//OUTPUT DATA SIGNAL TO THE BRIDGE.

	Reg#(Bit#(32))  rg_PRDATA           <- mkConfigReg(0);
	Reg#(Bit#(1))   rg_slave_to_APB_valid_data <- mkConfigReg(0);

//INPUT DATA SIGNAL FROM THE SLAVE.

	Wire#(Bit#(32)) wr_PRDATA            <- mkDWire(0);
	Wire#(Bit#(1))  wr_slave_to_APB_valid_data  <-mkDWire(0);

//INPUT RESPONSE SIGNALS FROM THE SLAVE.

	Wire#(Bit#(1))wr_slave_PREADY      <- mkDWire(1);
	Wire#(Bit#(1))wr_slave_PSLVERR     <- mkDWire(0);

//OUTPUT ADDRESS,CONTROL & DATA TO THE SLAVE

	Reg#(Bit#(32))  rg_PWDATA           <- mkConfigReg(0);       
	Reg#(Bit#(32))  rg_PADDR            <- mkConfigReg(0);
	Reg#(Bit#(1))   rg_PWRITE           <- mkConfigReg(0);      
	Reg#(Bit#(1))	rg_PENABLE	    <- mkConfigReg(0);
	Reg#(Bit#(1))	rg_PSELX	    <- mkConfigReg(0);
	Reg#(Bit#(1))	rg_prdata_ready	    <- mkConfigReg(0);

rule rl_IDLE_reset(rg_state==0||wr_proc_reset==1 || wr_HSELX==0 ||wr_HTRANS==2'b00 ||wr_HTRANS==2'b01);
	
	if(rg_PREADY==0 || (rg_PREADY==1 && !wr_input_fire))begin
		rg_PWDATA  		<=0;
		rg_PADDR   		<=0;
		rg_PWRITE  		<=0;
		rg_PENABLE 		<=0;
		rg_PSELX   		<=0;
		rg_state   		<=0;
		rg_PREADY  		<=wr_slave_PREADY;
		rg_bridge_deque  	<=0;
		//$display("**************IDLE TO IDLE***************");
	end
	else if(wr_input_fire)begin
		rg_state<=1;
		rg_PSELX		<=wr_HSELX;
		rg_PENABLE		<=0;
		rg_PADDR		<=wr_HADDR;
		rg_PWRITE		<=wr_HWRITE;
		rg_PREADY		<=wr_slave_PREADY;
		rg_bridge_deque		<=1;
		if(wr_HWRITE==0)begin
			rg_PWDATA		<=0;
			rg_slave_to_APB_valid_data  	<=wr_slave_to_APB_valid_data;
		end
		else begin
			rg_PWDATA		<=wr_HWDATA;
		end
		//$display("**************IDLE TO SETUP***************");	
	end	
endrule

rule rl_SETUP(rg_state==1);
		if(wr_proc_reset==1)begin
			rg_state   		<=0;
			rg_PWDATA  		<=0;
			rg_PADDR   		<=0;
			rg_PWRITE  		<=0;
			rg_PENABLE 		<=0;
			rg_PSELX   		<=0;
			rg_PREADY  		<=wr_slave_PREADY;
			rg_bridge_deque	<=0;

		//	$display("*************SETUP TO IDLE***************");
		end	
		else begin
			rg_state   		<=2;	
			rg_PWDATA  		<=rg_PWDATA;
			rg_PADDR   		<=rg_PADDR;
			rg_PWRITE  		<=rg_PWRITE;
			rg_PENABLE 	   	<=1;
			rg_PSELX   	   	<=rg_PSELX;
			rg_PREADY  	 	<=wr_slave_PREADY;
			rg_bridge_deque		<=0;
		//$display("************SETUP TO ACCESS***************");	
		end
endrule

rule rl_ACCESS(rg_state==2);

	if(wr_slave_PREADY==0)begin
		rg_state  		<= 2;
		rg_PREADY 		<=wr_slave_PREADY;
		//$display("*************ACCESS TO ACCESS***************");
	end
	else if (wr_input_fire && wr_HSELX==1)
	begin	
		rg_state  		<=1;
		rg_PSELX		<=wr_HSELX;
		rg_PENABLE		<=0;
		rg_PADDR		<=wr_HADDR;
		rg_PWRITE		<=wr_HWRITE;
		rg_PREADY		<=wr_slave_PREADY;
		rg_bridge_deque		<=1;
		if(wr_HWRITE==0)begin
			rg_PWDATA	<=0;
		end
		else begin
			rg_PWDATA		<=wr_HWDATA;
		end
		//$display("*************ACCESS TO SETUP***************");	
	end
	else if(wr_proc_reset==1 || wr_HSELX==0 || wr_HTRANS==2'b00 ||wr_HTRANS==2'b01)begin
		rg_state  		<=0;
		rg_PSELX		<=0;
		rg_PENABLE		<=0;
		rg_PADDR		<=0;
		rg_PWRITE		<=0;
		rg_PWDATA		<=0;
		rg_PREADY		<=wr_slave_PREADY;
		//$display("*************ACCESS TO IDLE***************");
	end
endrule

/* ready from master */

rule rl_master_ready;
 rg_prdata_ready <= wr_proc_HRDATA_read_ready;
endrule

rule rl_PSLVERR;
	if(wr_HTRANS==0 || wr_proc_reset==1)
		rg_PSLVERR<=0;
	else 
		rg_PSLVERR<=wr_slave_PSLVERR;

	//$display("rg_PREADY",rg_PREADY);
	//$display("rg_bridge_deque=%d",rg_bridge_deque);
endrule

rule rl_PRDATA;

	if(wr_proc_reset==1)begin
		rg_PRDATA 			<=	0;
		rg_slave_to_APB_valid_data  	<=	0;
		end
	else if(wr_proc_HRDATA_read_ready==0)begin
		rg_PRDATA			<=	rg_PRDATA;
		rg_slave_to_APB_valid_data	<=	rg_slave_to_APB_valid_data;
		end	
	else if(wr_slave_to_APB_valid_data==1 )	begin
		rg_PRDATA			<=	wr_PRDATA;
		rg_slave_to_APB_valid_data  	<=	wr_slave_to_APB_valid_data;
	end
	else begin
		rg_PRDATA			<=	0;
		rg_slave_to_APB_valid_data  	<=	0;
	end
endrule

/* ADDRESS & CONTROL SIGNALS FROM THE BRIDGE */
method Action _address_and_control_from_bridge(Bit#(32) _address,Bit#(1) _write,Bit#(3) _data_size, Bit#(3) _burst_type, Bit#(4) _prot_type,Bit #(2) _transfer_type, Bit #(1) _bridge_lock, Bit#(1) _Hselect);

	wr_HADDR     		<=  _address;	
	wr_HWRITE    		<=  _write;
	wr_HSIZE     		<=  _data_size;
	wr_HBURST    		<=  _burst_type;
	wr_HPROT     		<=  _prot_type;
	wr_HTRANS    		<=  _transfer_type;
	wr_HMASTLOCK 		<=  _bridge_lock;
	wr_HSELX     		<=  _Hselect;
	wr_input_fire		<=  True;
	//$display("Wr_hselx=%d",_Hselect);
endmethod

/* BUSY SIGNAL FROM BRIDGE */

method Action _busy_from_bridge(Bit#(1) _bridge_fifo_ready);

	wr_proc_HRDATA_read_ready <= _bridge_fifo_ready;

endmethod
/* RESET FROM THE PROCESSOR*/
method Action _reset_from_proc(Bit#(1) _proc_reset);

	wr_proc_reset   <= _proc_reset;

endmethod

/* DATA FROM THE SLAVE DEVICES */

method Action _data_from_slave(Bit#(32) _slave_data);

	wr_PRDATA     		<= _slave_data;
	wr_slave_to_APB_valid_data  	<= 1;

endmethod

/* DATA FROM THE BRIDGE */

method Action _data_from_bridge(Bit#(32) _bridge_data);

	wr_HWDATA    	     <= _bridge_data;
endmethod

/* RESPONSE SIGNALS FROM THE SLAVES */

method Action _response_from_slave(Bit#(1) _slave_ready,Bit#(1) _slave_error);

	wr_slave_PREADY    <= _slave_ready;
	//rg_PREADY	   <= _slave_ready;
	wr_slave_PSLVERR   <= _slave_error;
//	rg_PSLVERR	   <= _slave_error;
	//$display("slave_ready=%d",_slave_ready);

endmethod



method Bit#(1) pSELX_();
	return rg_PSELX;
endmethod

method Bit#(1) pENABLE_();
	return rg_PENABLE;
endmethod

method Bit#(1) pWRITE_();
	return rg_PWRITE;
endmethod

method Bit#(32) pADDR_();
	return rg_PADDR;
endmethod

method Bit#(1) pREADY_();
	return rg_PREADY;
endmethod

method Bit#(32) pRDATA_();
	return rg_PRDATA;
endmethod

method Bit#(32) pWDATA_();
	return rg_PWDATA;
endmethod

method Bit#(1) pSLVERR_();
	return rg_PSLVERR;
endmethod

method Bit#(1) bridge_deque_();
	return rg_bridge_deque;
endmethod

method Bit#(1) valid_slave_data_();
	return rg_slave_to_APB_valid_data;
endmethod

method Bit#(1) pBUSY_();
	return rg_prdata_ready;
endmethod

endmodule
endpackage

