/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

/* ==================================================================================
 
 RISC-V Defined Privilege Levels
 RISC-V ISA defines four Privilege Levels with Abbreviations and Encoding as below
 Level Encoding   Name  	 Abbreviation
 ----- --------   ----       ------------
   0		00    User 			U
   1		01	  Supervisor	S
   2		10	  Hypervisor	H
   3		11	  Machine		M
 This package defines functions and types necessary to implement User-Level 
 and Machine-Level Privileges

=====================================================================================*/

package ISA_Defs_PRV;

import ISA_Defs :: *;	// Needed for importing XPRLEN

// Privilege Level Declaration
typedef enum {U, S, H, M} Privilege_Level deriving (Bits, Eq);

// User-Level Control and Status Register Data
typedef Bit #(XPRLEN) UCSR_Data;
typedef Bit #(XPRLEN) UCSR_Data_64;

// Machine-Level Control and Status Register Data
typedef Bit #(XPRLEN) MCSR_Data;

// User-Level Read-Only CSRs
typedef enum {
				/*User Counter/Timers*/ CYCLE, TIME, INSTRET, CYCLEH, TIMEH, INSTRETH
} UCSR_ReadOnly deriving (Bits, Eq);

// Machine-Level CSRs that provide only Read-Only access
typedef enum {
				/*Machine Information Registers*/ MCPUID, MIMPID, MHARTID
} MCSR_ReadOnly deriving (Bits, Eq);

// Machine-Level CSRs that provide both Read-Write access
typedef enum {
				/*Machine Trap Status CSR*/ MSTATUS, MTVEC, MTDELEG, MIE, MTIMECMP,
				/*Macine Timers and Counters*/ MTIME, MTIMEH,
				/*Machine Trap Handling*/ MSCRATCH, MEPC, MCAUSE, MBADADDR, MIP,
				/*Machine Protection and Translation*/ MBASE, MBOUND, MIBASE, MIBOUND, MDBASE, MDBOUND
				/*Machine Read-Write Shadow of Hypervison Read-Only Registers - Not implemented currently*/
				/*Machine Host-Target Interface (Non-Standard Berkeley Extension) - Not implemented currently*/
} MCSR_ReadWrite deriving (Bits, Eq);

// Utility type definition. Used to identify nature of User-Level CSR being addressed
typedef union tagged {
	UCSR_ReadOnly ReadOnly;
	Bool		  Undefined;
} UCSR_Address_Map deriving (Bits, Eq);

// Utility type definition. Used to identify nature of Machine-Level CSR being addressed
typedef union tagged {
	MCSR_ReadWrite ReadWrite;
	MCSR_ReadOnly  ReadOnly;
	Bool		   Undefined;
} MCSR_Address_Map deriving (Bits, Eq);

// Fuction to identify the User-Level CSR being addressed by 'ucsr_address' field
// Return one of the following three values:
// 		- tagged ReadOnly  (with identity of RO USCR being accessed) if one of the Read-Only UCSRs was addressed
// 		- tagged ReadWrite (with identity of RW USCR being accessed) if one of the Read-Write UCSRs was addressed
// 		- tagged Undefined if the address does not map to any valid UCSR
// In this implementation however, only RO UCSRs being used
function UCSR_Address_Map map_ucsr (Bit #(12) ucsr_address);
	UCSR_Address_Map ucsr_map = ?;
	case (ucsr_address) matches
		// User Counter/Timers
		12'hC00: ucsr_map = tagged ReadOnly CYCLE;
		12'hC01: ucsr_map = tagged ReadOnly TIME;
		12'hC02: ucsr_map = tagged ReadOnly INSTRET;
		12'hC80: ucsr_map = tagged ReadOnly CYCLEH;
		12'hC81: ucsr_map = tagged ReadOnly TIMEH;
		12'hC82: ucsr_map = tagged ReadOnly INSTRETH;
	endcase
	return ucsr_map;
endfunction

// Fuction to identify the Machine-Level CSR being addressed by 'mcsr_address' field
// Return one of the following three values:
// 		- tagged ReadOnly  (with identity of RO MSCR being accessed) if one of the Read-Only MCSRs was addressed
// 		- tagged ReadWrite (with identity of RW MSCR being accessed) if one of the Read-Write MCSRs was addressed
// 		- tagged Undefined if the address does not map to any valid MCSR
function MCSR_Address_Map map_mscr (Bit #(12) mcsr_address);
	MCSR_Address_Map mcsr_map = ?;
	case (mcsr_address)
		// Machine Information Registers
		12'hf00: mcsr_map = tagged ReadOnly MCPUID;
		12'hf01: mcsr_map = tagged ReadOnly MIMPID;
		12'hf10: mcsr_map = tagged ReadOnly MHARTID;

		// Machine Trap Status
		12'h300: mcsr_map = tagged ReadWrite MSTATUS;
		12'h301: mcsr_map = tagged ReadWrite MTVEC;
		12'h302: mcsr_map = tagged ReadWrite MTDELEG;
		12'h304: mcsr_map = tagged ReadWrite MIE;
		12'h321: mcsr_map = tagged ReadWrite MTIMECMP;

		// Machine Timers and Counters
		12'h701: mcsr_map = tagged ReadWrite MTIME;
		12'h741: mcsr_map = tagged ReadWrite MTIMEH;

		// Machine Trap Handling
		12'h340: mcsr_map = tagged ReadWrite MSCRATCH;
		12'h341: mcsr_map = tagged ReadWrite MEPC;
		12'h342: mcsr_map = tagged ReadWrite MCAUSE;
		12'h343: mcsr_map = tagged ReadWrite MBADADDR;
		12'h344: mcsr_map = tagged ReadWrite MIP;

		// Machine Protection and Translation
		12'h380: mcsr_map = tagged ReadWrite MBASE;
		12'h381: mcsr_map = tagged ReadWrite MBOUND;
		12'h382: mcsr_map = tagged ReadWrite MIBASE;
		12'h383: mcsr_map = tagged ReadWrite MIBOUND;
		12'h384: mcsr_map = tagged ReadWrite MDBASE;
		12'h385: mcsr_map = tagged ReadWrite MDBOUND;

		default: mcsr_map = tagged Undefined True;
	endcase
	return mcsr_map;
endfunction

// Exception Declaration
// The following exceptions are defined as per Machine cause register (mcause) values
// These exceptions are defined only when not caused by an interrupt
typedef enum {
				/*Instruction Address Misaligned*/ MCAUSE_INSTRN_ADDR_MISALN,
				/*Instruction Access Fault*/ 	   MCAUSE_INSTRN_ACCESS_FAULT,
				/*Illegal Instruction*/ 		   MCAUSE_ILLEGAL_INSTRN,
				/*Breakpoint*/ 					   MCAUSE_BREAKPOINT,
				/*Load Address Misaligned*/ 	   MCAUSE_LOAD_ADDR_MISALN,
				/*Load Access Fault*/ 			   MCAUSE_LOAD_ACCESS_FAULT,
				/*Store/AMO Address Misaligned*/   MCAUSE_STORE_ADDR_MISALN,
				/*Store/AMO Access Fault*/  	   MCAUSE_STORE_ACCESS_FAULT,
				/*Environment Call From U-Mode*/   MCAUSE_ENV_CALL_U,
				/*Environment Call From S-Mode*/   MCAUSE_ENV_CALL_S,
				/*Environment Call From H-Mode*/   MCAUSE_ENV_CALL_H,
				/*Environment Call From M-Mode*/   MCAUSE_ENV_CALL_M
} MCause_Exception_NoInt deriving (Bits, Eq);

// These exceptions are defined only when caused by an interrupt
// Software interrupt and Timer interrupt are the pre-defined exceptions caused due to interrupts
typedef enum {
				/*Software Interrupt*/	MCAUSE_SOFT_INT,
				/*Timer Interrupt*/ 	MCAUSE_TIMER_INT
} MCause_Exception_Int deriving (Bits, Eq);

endpackage
