/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package Testbench;

/* ================================================================

// Top module: mkTestbench
// -----------------------
// module mkTestbench initializes the system by reseting CPU, setting initial Program Counter (PC) and initializing Instruction and Data Memories
// It then sets up the interconnection between CPU, various memories and peripherals. Currently, ARM's AMBA AHB bus is being used
// Testbench currently supports execution in two modes: 1. DEBUG Environment 2. VERIFICATION Environment

// DEBUG Environment:  
// ------------------
// This module initializes Memory_model and CPU and provides 
// testbench for the Processor. Also establishes the interconnection
// between CPU and MEMORY

================================================================ */

// ================================================================

// Bluespec libraries

import Vector       :: *;
import FIFOF        :: *;
import GetPut       :: *;
import ClientServer :: *;
import Connectable  :: *;
import StmtFSM      :: *;

// ================================================================ 

// ================================================================
// Project imports

import TLM2	          :: *;	// Using local copy.
import AHB			  :: *;	// Using local copy. 
import Utils          :: *;
import Req_Rsp        :: *;
import Sys_Configs    :: *;
import C_import_decls :: *;

import ISA_Defs       :: *;
import CPU            :: *;
import Memory_Model   :: *;
import Fabric		  :: *;

`include "TLM.defines"
`include "RVC.defines"
`include "macro.defines"
// ================================================================
// Interactive commands from the console
// Commands are returned by the imported C function: c_console_command ()
// which returns a Vector #(10, Bit #(64))
// [0] is the arg count (>= 1)
// [1] is the command, see codes below
// [2..9] are the optional arguments (up to 8 args)

Bit #(32) cmd_continue   = 0;
Bit #(32) cmd_dump       = 1;
Bit #(32) cmd_quit       = 2;
Bit #(32) cmd_reset      = 3;
Bit #(32) cmd_step       = 4;
Bit #(32) cmd_step_until = 5;
Bit #(32) cmd_verbosity  = 6;    // arg: verbosity level
Bit #(32) cmd_dump_mem   = 7;

// ================================================================
// Testbench module

(* synthesize *)
module mkTestbench (Empty) ;

// CPU interfaces are of type TLM
// CPU has two interfaces - masters
// To recieve these interfaces we must have two AHBMasterXActor
// This will convert the request to AHBFabricMaster that contains an arbiter
// on the other side, we need a AHBSlaveXActor to convert the request back to TLMRecvIFC
// In between, we must have a bus that will connect the master fabric with the slave fabric

// easy
// make Vector #(n, AHBMasterXActor(`TLM_RR_AHB, `TLM_PRM_REQ)) ahb_master <- replicate(mkAHBMaster);

// make Vector #(m, AHBSlaveXActor(`TLM_RR_AHB, `TLM_PRM_RSP)) ahb_slave <- replicate(mkAHBSlave(addr_match));
// Empty ahb_bus <- mkAHBBus (ahb_master.AHBFabricMaster, ahb_slave.AHBFabricSlave);
// make connections between cpu and ahb_master.tlmrecv interface and on the slave side as well

   // --------
   // File Handles

   // Architectural Register File dump target
   Reg #(File) file_arf_dump <- mkRegU;

   // --------
   // Module Instantiation

   // Top Module for the E-Class processor
   CPU_IFC cpu <- mkCPU_Model;

/*
------------------------------------------------------------------------------------
   // Note on Memory Module
   // ---------------------
   // Currently, memory has been implemented as a REGFILE with 32-bit address range, each entry being 32-bit wide
   // These configurations could be found and modified in SysConfig file
   // This memory module does not support mis-aligned LOADs or STOREs
   // Word size LOAD/STOREs should be word aligned. Half-word size LOAD/STOREs should be word or half-word aligned. Byte size operations could be performed at any address
   // Memory instantiation must receive the following parameters: 1. Initialization file name 2. Starting (Base) address 3. Memory initialization size
   // The last two parameters are not being used as currently a RegFileFullLoad is done. This initializes the all Regfile registers (2^32 currently)
   // Another implementation (Not used currently) allows memory to be dynamically instantiated through C constructs
------------------------------------------------------------------------------------
*/
   // Instruction Memory
   Memory_IFC imem <- mkMemory_Model(`INSTRUCTION_MEM_INIT_FILE, imem_base_addr, imem_size);

   // Data Memory
   Memory_IFC dmem <- mkMemory_Model(`DATA_MEM_INIT_FILE, dmem_base_addr, dmem_size);

   // Connection between CPUs instruction memory port and Instruction memory
   mkConnection (cpu.imem_ifc, imem.bus_ifc);

   // Connection between CPUs data memory port and Data memory
   mkConnection (cpu.dmem_ifc, dmem.bus_ifc);

  /* // ----------- Currently not using it -------------------
		// Issue of multiple simultaneous issues is to be resolved
   // AHB Bus Interconnect

	// Helper functions
	function Bool addr_match_mem_instr (AHBAddr #(`TLM_PRM_RSP_MEM) value);
		return ((value >= imem_base_addr && value <= imem_max_addr)?True:False);
	endfunction
	
	function Bool addr_match_mem_data (AHBAddr #(`TLM_PRM_RSP_MEM) value);
		return ((value >= dmem_base_addr && value <= dmem_max_addr)?True:False);
	endfunction
	
	function AHBMasterFabric funct1(AHBMasterActor a);
		return a.fabric;
	endfunction
	
	function AHBSlaveFabric funct2(AHBSlaveActor a);
		return a.fabric;
	endfunction
	
	// Instantaite transactors for TLM Masters and Slaves
	Vector #(Max_Initiators, AHBMasterActor) masterX <- replicateM (mkAHBMaster);

	// Make Slaves
	AHBSlaveActor slave_mem_instr <- mkAHBSlave(addr_match_mem_instr);
	AHBSlaveActor slave_mem_data  <- mkAHBSlave(addr_match_mem_data);

	Vector #(Max_Targets, AHBSlaveActor) slaveX = newVector;
	slaveX[bus_mem_instr_port] = slave_mem_instr;
	slaveX[bus_mem_data_port]  = slave_mem_data;

	// Instantiate bus interface
	Empty ahb_bus <- mkAHBBus (map(funct1, masterX), map(funct2, slaveX));
   
	// Establish connectivity between Bus, Masters and Slaves
	mkConnection(cpu.imem_ifc, masterX[bus_cpu_instr_port].tlm);	// bus_cpu_instr_port = 0
	mkConnection(cpu.dmem_ifc, masterX[bus_cpu_data_port].tlm);		// bus_cpu_data_port  = 1
   
	mkConnection(slaveX[bus_mem_instr_port].tlm, mem.bus_ifc [mem_instr_port]);		// bus_mem_instr_port = 0
    mkConnection(slaveX[bus_mem_data_port].tlm, mem.bus_ifc [mem_data_port]);		// bus_mem_data_port = 1

	rule rl_debug_masters;
		$display($time, " BUS: Master 0: Addr: %h", masterX[0].fabric.bus.hADDR);
		$display($time, " BUS: Master 0: BusREQ %d", masterX[0].fabric.arbiter.hBUSREQ);
		//$display($time, " BUS: Master 0: BusLOCK %d", masterX[0].fabric.arbiter.hLOCK);
		//$display($time, " BUS: Master 0: ");
		$display($time, " BUS: Master 1: Addr: %h", masterX[1].fabric.bus.hADDR);
		$display($time, " BUS: Master 1: BusREQ %d", masterX[1].fabric.arbiter.hBUSREQ);

		$display($time, " BUS: Slave 0: Ready: %d", slaveX[0].fabric.bus.hREADY);
		$display($time, " BUS: Slave 1: Ready: %d", slaveX[1].fabric.bus.hREADY);
	endrule
*/
   
   // ----------
   // Check requested mode of operation of the CPU
   // Structure - `ifdef CPU_MODE_NORMAL run cpu in normal mode
   //		      `else
   //                  `ifdef CPU_MODE_DEBUG run cpu in debug mode
   //					`endif
   //					`ifdef CPU_MODE_VERIFICATION run cpu in verification mode
   //					`endif
   //			  `endif // for CPU_MODE_NORMAL

   // If the following condition is met, the CPU executes in Normal Mode
   // This mode allows execution of instructions as and when they arrive. At all other times, CPU must run ideally (perform NOPs?)
   `ifdef CPU_MODE_NORMAL
	   rule rl_dummy; $display("NORMAL"); endrule

	`else

   // ============= COMMON DEFINTIONS ====================================

   // -------
   // Miscellanious registers
  
   // Loop iterators
   Reg #(Bit #(6)) rg_xi <- mkRegU;
   Reg #(Bit #(6)) rg_xj <- mkRegU;

   // Registers needed for generating output similar to that generated by SPIKE ISS
   Bit #(64) dump_pc = zeroExtend((cpu.read_exec_pc));
   Reg #(Bit #(64)) rg_dump_gpr <- mkRegU;

   // Fuction to display Architectural Regfile Entry
   function Action show64b (String pre, GPR_Data x, String post);
      action
	 $write ("%s", pre);
	 Bool leading = True;
	 for (Integer j = 7; j > 0; j = j - 1) begin
	    if (leading && (x [31:28] == 0))
	       $write (" ");
	    else begin
	       $write ("%1h", x [31:28]);
	       leading = False;
	    end
	    x = x << 4;
	 end
	 $write ("%1h%s", x [31:28], post);
      endaction
   endfunction

   // Statement to dump current CPU state to console
   Stmt dump_arch_state =
      seq
	 action
	    let pc      = cpu.read_pc;
	    let instret = cpu.read_instret;
	    let cycle   = cpu.read_cycle;
		$display;
	    $write ("    instret: %0d, CPU cycles: %0d", instret, cycle);
	    show64b ("    PC: ", pc, "\n");
		$display;
	 endaction
	 for (rg_xj <= 0; rg_xj < 32; rg_xj <= rg_xj + 4) seq
	    $write   ("    %2d:", rg_xj);
	    show64b (" ", cpu.read_gpr (truncate (rg_xj)), "");
	    show64b (" ", cpu.read_gpr (truncate (rg_xj) + 1), "");
	    show64b (" ", cpu.read_gpr (truncate (rg_xj) + 2), "");
	    show64b (" ", cpu.read_gpr (truncate (rg_xj) + 3), "\n");
	 endseq
      endseq;


   // Statement to dump current CPU state to file
   Stmt dump_arch_state_to_file =
   		seq
	   action
		  let pc = cpu.read_pc;
		  $fwrite (file_arf_dump, "PC = %h\n", (dump_pc));
	   endaction
		  
		  for (rg_xi <= 1; rg_xi < 32; rg_xi <= rg_xi + 1) seq
			rg_dump_gpr <= signExtend(cpu.read_gpr (truncate(rg_xi)));
		  	$fwrite (file_arf_dump, "REG %2d %h\n", rg_xi, rg_dump_gpr);
		  endseq

	   action
		  $fwrite (file_arf_dump, "\n");
	   endaction
		endseq;

   // Statement to initialize CPU
   Stmt initialize_cpu
   = seq
	action // Reset CPU
	   $display ($time, " TB: Initializing CPU");
	   cpu.reset;	// True sets mode to debug
	endaction
	
	action // Set initial PC
	   let start_pc = fromInteger(valueOf(`PC_INIT_VALUE));
	   cpu.write_pc (start_pc);
	   $display ($time, " TB: Setting initial PC to: %0h", start_pc);
	endaction
   endseq;
   
   Stmt initialize_memory = seq
	action // Display initial memory allocation
	   $display ($time, " TB: Initializing Instruction memory [%0h..%0h]", imem_base_addr, 32'hffffffff/*imem_max_addr*/);
	   $display ($time, " TB: Initializing Data memory [%0h..%0h]", dmem_base_addr, 32'hffffffff/*dmem_max_addr*/);
	endaction
   endseq

   // ============= END: COMMON DEFINTIONS ===============================

   // If the following condition is met, the CPU executes in Software Debug mode
   // This mode allows step-by-step execution of instruction and dumping of the Architectural Register File
   	`ifdef CPU_MODE_DEBUG;

   // =================== SOFT-DEBUG ENVIRONMENT ====================================

   // ----------------
   // Main behavior: process a queue of interactive commands
   Reg #(Vector #(10, Bit #(32))) rg_console_command <- mkRegU;
   Bit #(32) console_argc     = rg_console_command [0];
   Bit #(32) console_command  = rg_console_command [1];
   Bit #(32) console_arg_1    = rg_console_command [2];
   Bit #(32) console_arg_2    = rg_console_command [3];
   Bit #(32) console_arg_3    = rg_console_command [4];
   Bit #(32) console_arg_4    = rg_console_command [5];
   Bit #(32) console_arg_5    = rg_console_command [6];
   Bit #(32) console_arg_6    = rg_console_command [7];
   Bit #(32) console_arg_7    = rg_console_command [8];
   Bit #(32) console_arg_8    = rg_console_command [9];

   // Miscellanious register
   Reg #(Bit #(32)) rg_addr <- mkRegU;
   
   // An automatic Finite-State-Machine emulates the testbench for Soft-Debug mode
   // Execution starts from the first clock cycle
   // CPU, and Instruction and Data Memories are initialized. Thereafter, the console waits for inputs provided through terminal to proceed
   mkAutoFSM (
    seq
	
	 // Initialize CPU
	 initialize_cpu;

	 // Initialize Memory
	 initialize_memory;

	 // Run
	 while (True) seq
	    action // Read command from console
	       let cmd <- c_get_console_command;
	       rg_console_command <= cmd;
	    endaction

		// If command is continue, execute all instructions without breaking
	    if (console_command == cmd_continue) seq
	       action
		  Maybe #(GPR_Data) mpc = (  (console_argc == 1)
					   ? tagged Invalid
					   : tagged Valid console_arg_1);
		  cpu.run_continue (mpc);
	       endaction
	       display_stop_reason (" TB: Stop reason: ", cpu.stop_reason, "\n");
	       dump_arch_state;
	    endseq

		// If command is dump, dump the Architectural Register File (here, dump to console)
	    else if (console_command == cmd_dump)
	       dump_arch_state;

		// If command is quit, terminate execution
	    else if (console_command == cmd_quit)
	       break;

		// If commnad is reset, reset CPU to initial state
	    else if (console_command == cmd_reset)
	       initialize_cpu;

		// If command is step, execute the next instruction to completion
		// Currently, even for instructions that take multiple cycles to complete (eg. LOAD/STORE), the execution terminates after the first cycle
		// @TODO this behaviour may be fixed by using 'rg_instret' of module CPU if the desired behaviour is to run the instruction to completion
	    else if (console_command == cmd_step) seq
	       action
		  Maybe #(GPR_Data) mpc = (  (console_argc == 1)
					   ? tagged Invalid
					   : tagged Valid console_arg_1);
		  cpu.run_step (mpc);
	       endaction
	       action
		  display_stop_reason (" TB: Stop reason: ", cpu.stop_reason, "\n");
		  $display ("");
	       endaction
	    endseq

		// If command is step_until, execute 'n' instructions. 'n' is a command line argument
	    else if (console_command == cmd_step_until) seq
	       while ((cpu.read_instret < console_arg_1) && (cpu.stop_reason != CPU_STOP_EXIT))
		  cpu.run_step (tagged Invalid);
	       action
		  display_stop_reason (" TB: Stop reason: ", cpu.stop_reason, "\n");
		  $display ("");
	       endaction
	       dump_arch_state;
	    endseq

		// If command is verbosity, change the verbosity of the CPU to 'v'. 'v' is a command line argument
		// 'verbosity' is a variable used to control the amount of messages printed during execution, mainly used for debugging purposes
		// if 'verbosity' is set to 0, no messages will be displayed on the console (desired behaviour)
	    else if (console_command == cmd_verbosity) action
	       int v = unpack (truncate (console_arg_1));
	       $display ($time, " TB: Setting verbosity to %0d", v);
	       cpu.set_verbosity (v);
	    endaction

		// If command is dump_memory, dump the data memory region in the range specified through command line (here, to the console)
	    else if (console_command == cmd_dump_mem)
	       for (rg_addr <= (console_arg_1 & 32'hFFFF_FFFC);
		    rg_addr < console_arg_2;
		    rg_addr <= rg_addr + 4)
	       seq
		  cpu.req_read_memW (rg_addr);
		  action
		     let d <- cpu.rsp_read_memW;
		     $display ($time, " TB: %08h: %08h", rg_addr, d);
		  endaction
	       endseq

		// If command is unknown, print error message and ignore the command
	    else
	       $display ("Error: Ignored unknown command: %0d", console_command);
	 endseq
      endseq
      );

	// ==================== END: SOFT DEBUG ENVIRONMENT ================================

	`endif // CPU_MODE_DEBUG

    // ----------
    // Check requested mode of operation of the CPU
    // If the following condition is met, the CPU executes in Verification mode
	// This mode allows the CPU to run to completion (end of instruction stream) and dump the Architectural Register File and Data Memory (here, to dump files)

	`ifdef CPU_MODE_VERIFICATION;
	
	// ==================== VERIFICATION ENVIRONMENT =====================================

	// Miscellanious registers
	Reg #(Bit #(32)) rg_prev_instret <- mkReg(0); 		// Number of instructions retired before current step
	Reg #(Int #(32)) rg_instret <- mkReg(0);					// Number of instructions retired after current step

	mkAutoFSM (
		seq
			initialize_cpu;
			initialize_memory;

			action
				// Open Architectural Register File dump target file with write access
				File file_handle <- $fopen (`FILE_REG_DUMP, "w");

				// Validate dump target
				if (file_handle == InvalidFile) action // Exit if invalid
					$display($time, " TB: Cannot open file '%s' for dumping state. Exiting!", `FILE_REG_DUMP);
					$finish();
				endaction				
				else  // If valid: update handle
					file_arf_dump <= file_handle;

			endaction

			// Execute all instructions untill the end of instruction stream
			// End of instruction stream is currently marked by 'illegal' instruction (0x0000006F)
			// One instruction is retired at a time using 'run_step' method of the CPU Debug interface
			// At the end of each step, current state of the Architectural Register File is printed to a file
			while (True) seq
				rg_prev_instret <= cpu.read_instret;
				cpu.run_step(tagged Invalid);
				if(cpu.read_instret - rg_prev_instret > 0) seq // IF condition ensures redundant states are not printed
														// Useful for multi-cycle instructions and when a branch instruction flushes the pipeline
					rg_instret <= unpack(cpu.read_instret);	   
					if (cpu.read_exec_pc >= 'h200) dump_arch_state_to_file; // IF condition normalized output according to Spike ISS
				endseq
			endseq
		endseq
	);

    // ==================== END: VERIFICATION ENVIRONMENT ================================

	`endif // CPU_MODE_VERIFICATION
	`endif // CPU_MODE_NORMAL
endmodule

// ================================================================

endpackage: Testbench
