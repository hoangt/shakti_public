/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package CPU_Execute;

// ================================================================

// This package models Execute stage of a RISC-V CPU
// Standard extension for Multiplication and Division (RV32M) is
// supported in addition to RISC-V Base Integer Instruction Set RV32I
// Architectural regfile is instantiated in this module. Therefore, operand fetch is done here as well
// Execute unit performs the following operation depending upon the type of instruction
// Integer Computational Instruction: Arithmetic and Logic operations on operands
// Data Transfer Instruction: Address calculation and operand manipulation
// Branch Instruction: Branch evaluation and target address calculation

// ================================================================
// Bluespec libraries

import RegFile      :: *;
import FIFOF        :: *;
import DefaultValue :: *;
import SpecialFIFOs :: *;
import DefaultValue :: *;

// ================================================================
// Project imports

import TLM2		    :: *;	// Using local copy
import Req_Rsp      :: *;
import Sys_Configs  :: *;
import Vector		:: *;

import ISA_Defs     :: *;
import ISA_Defs_PRV :: *;   // Privilege ISA defintions

`include "TLM.defines"
`include "RVC.defines"
// ================================================================


// structure to pass load/store information to function fn_mk_ls
typedef struct {
	TLMCommand cmd;
	GPR_Data   data;
	Addr 	   addr;
	UInt #(8)  bsize;
	LS_Remark  remark;
} LS_temp deriving (Bits);

interface Execute_IFC;
	method Action							write_executeData (IDEX_Data   execData);			// Input data. Data Forwarded from Decode Unit
	method Action							write_gpr         (RegName rd, GPR_Data data);		// Update an entry in Archtictural Register File
	method Action							write_verbosity   (int verbosity);					// Update verbosity
	method Action 							write_cpu_stopped (Bool cpu_stopped);   			// Current CPU State. Stopped or Fetching

	method GPR_Data 						read_gpr          (RegName regname);				// Read a value from ARF
	method GPR_Data							read_exec_pc;										// Returns PC of instruction being executed
	method Wire #(Maybe #(CommitData))  	read_cd;											// Returns data to be committed
	method Wire #(CPU_Stop_Reason)	        read_stop_reason;									// Return reason why CPU is currently stopped
	method UCSR_Data_64						read_instret;										// Return the value of User-Level CSR 'instret'
	method UCSR_Data_64						read_cycle;											// Return the value of User-Level CSR 'cycle'
endinterface

module mkExecute (Execute_IFC);
    
	// Architectural (General-Purpose) Register File
    RegFile #(RegName, GPR_Data) regfile <- mkRegFileFullLoad("regfile_initialize");
   
	// Utility wires and registers
	Wire #(GPR_Data)        	wr_pc			<- mkWire;			// PC of currently executed instruction
	Reg #(GPR_Data)		    	rg_last_exec_pc	<- mkRegU;			// PC of last instruction executed. Used for verification purposes only
    Wire #(Bool)	        	wr_instrn_valid <- mkWire;			// Set if new instruction has arrived. Ensures rules do not keep on firing for old instruction in case pipeline is stalled
	Wire #(CPU_Stop_Reason)	 	wr_stop_reason	<- mkBypassWire;	// Reason why CPU is currently stopped
	Wire #(Bool)				wr_cpu_stopped	<- mkWire;			// Current State of CPU. Stopped or Fetching
	Wire #(int)		  			wr_verbosity	<- mkBypassWire;

	Wire #(Maybe #(CommitData)) wr_cd 	    	<- mkWire;	// Data passed back to CPU module to be committed

	// The below five variables of Maybe type are exclusive
	// Only one will be valid at a time
	// Depending upon their validity, request of either write-back, load-store or jump/branch is made
	Wire #(Maybe #(GPR_Data))    val_wb    <- mkDWire(tagged Invalid);	// Valid if an ALU operation was performed. Holds the value to be written back
	Wire #(Maybe #(GPR_Data))    val_wb_3  <- mkDWire(tagged Invalid);	// Not used. Intended to be Used with RV Compressed extension
	Wire #(Maybe #(LS_temp))     val_ls    <- mkDWire(tagged Invalid); 	// Valid if Load/Store was requested. Holds data memory address, data and size of data to be read or written
	Wire #(Maybe #(RequestJump)) val_jmp   <- mkDWire(tagged Invalid);	// Valid if Branch instruction was encountered. Holds the branch target address and value to be written to Link Register
	Wire #(Maybe #(Bool)) 		 val_halt  <- mkDWire(tagged Invalid);	// Valid if halt was requested (i.e. instruction '0000006F' was encountered). Used only for verification purpose

	// Output of decoder. Used by module execute as input
	Wire #(Opcode)    opcode  <- mkWire;
	Wire #(RegName)   rs1	  <- mkWire;
	Wire #(RegName)   rs2	  <- mkWire;
	Wire #(RegName)   rd 	  <- mkWire;
	Wire #(Bit #(12)) imm_I	  <- mkWire;
	Wire #(Bit #(12)) imm_S	  <- mkWire;
	Wire #(Bit #(13)) imm_B	  <- mkWire;
	Wire #(Bit #(32)) imm_U	  <- mkWire;
	Wire #(Bit #(21)) imm_J	  <- mkWire;
	Wire #(Bit #(3))  funct3  <- mkWire;
	Wire #(Bit #(7))  funct7  <- mkWire;
	Wire #(Bool)	  wr_illegal_instruction <- mkWire;

	// Values of Source register 1 and 2
	Wire #(GPR_Data) v1 <- mkWire;
	Wire #(GPR_Data) v2 <- mkWire;

	// Function to update the Architectural (General-Purpose) Register File
	function GPR_Data fn_read_gpr (RegName r);
      return ((r == 0) ? 0 : regfile.sub (r));
   	endfunction
	
   	// ---------
    // Utility functions

	// Creates and returns a write back packet.
	// Contains destination register name and data
	function RequestWriteBack fn_mk_wb (RegName regname, GPR_Data data);
		return RequestWriteBack { data: data, rd: regname };
	endfunction

	// Creates and returns a Load-Store packet.
	// Contains memory request packet for Data Memory.
	function ReqLS fn_mk_ls (RegName rwb, LS_temp lst, TLMId #(`TLM_PRM_REQ_CPU) tid);
		
		// Create request description for data memory request
		Req_Desc_CPU req_desc  	= defaultValue;
		req_desc.command		= lst.cmd;
		req_desc.addr 			= lst.addr;
		req_desc.data 			= lst.data;
		req_desc.transaction_id = tid;
		req_desc.burst_size 	= reqSz_bytes_i(lst.bsize);

		Req_CPU req    			= tagged Descriptor req_desc;
	
		// Create CommitData for load store requests
		ReqLS reqls				= ReqLS { tlmreq: req, rd: rwb, remark: lst.remark };
	
		return reqls;
	endfunction

	function fn_report_exec_instr;
		case (opcode)
			op_IMM    : $display ("OPCODE: %b FUNCT3: %h RD: %d RS1: %d IMM: %h", opcode, funct3, rd, rs1, imm_I);
			op_RR     : $display ("OPCODE: %b FUNCT7: %h FUNCT3: %h RD: %d RS1: %d RS2: %d", opcode, funct7, funct3, rd, rs1, rs2);
			op_LOAD   : $display ("OPCODE: %b FUNCT3: %h RD: %d RS1: %d IMM: %h", opcode, funct3, rd, rs1, imm_I);
			op_STORE  : $display ("OPCODE: %b FUNCT3: %h RS1: %d RS2: %d IMM: %h", opcode, funct3, rs1, rs2, imm_S);
			op_BRANCH : $display ("OPCODE: %b FUNCT3: %h RS1: %d IMM: %h", opcode, funct3, rs1, rs2, imm_B);
			op_JALR	  : $display ("OPCODE: %b FUNCT3: %h RD: %d RS1: %d IMM: %h", opcode, funct3, rd, rs1, imm_I);
			op_JAL	  : $display ("OPCODE: %b RD: %d IM: %h", opcode, rd, imm_J);
			op_LUI	  : $display ("OPCODE: %b RD: %d IMM: %h", opcode, rd, imm_U);
			op_AUIPC  : $display ("OPCODE: %b RD: %d IMM: %h", opcode, rd, imm_U);
		endcase
	endfunction

	// ---------
	// Execute Instructions
	
	// Fetch operands
	rule rl_operand_fetch (wr_instrn_valid);
		v1 <= fn_read_gpr(rs1);
		v2 <= fn_read_gpr(rs2);
	endrule

	// Set PC of instruction last executed
	rule rl_update_last_exec_pc;
		rg_last_exec_pc <= wr_pc;
	endrule

	// Halt CPU if an illegal instruction was encountered. Used for Verification purposes
	rule rl_halt (wr_illegal_instruction && wr_instrn_valid);
		val_halt <= tagged Valid True;
		wr_stop_reason <= CPU_STOP_EXIT;
		$finish(); 
	endrule

	// The following function performs right arithmetic shift on 'in' by the amount 'amt'
	// It is currently defined only for 32 bit values
	function GPR_Data fn_right_arith_shift (GPR_Data in, Bit #(5) amt);
		Bit #(64) _in;
		_in = (in[31] == 0) ? {zeroExtend(in)} : {signExtend(in)};
		return ((_in >> amt)[31:0]);
	endfunction

	// Execute instructions with immediate encoding
	rule rl_exec_IMM (opcode == op_IMM && wr_instrn_valid);
		Bit #(5) shift_amt = imm_I[4:0];
		Bit #(7) right_shift_type = imm_I[11:5];

		GPR_Data_S  sign_v1  = unpack(v1);
		GPR_Data_U  usign_v1 = unpack(v1);
	
		Bool no_match_1 = False, no_match_2 = False;
		case (funct3)
			f3_ADDI  : val_wb <= tagged Valid (v1 + signExtend (imm_I));
			f3_SLTI  : begin
						if (sign_v1 < unpack(signExtend(imm_I))) val_wb <= tagged Valid (1);
						else val_wb <= tagged Valid (0);
					  end
			f3_SLTIU : begin
						if (usign_v1 < unpack(signExtend(imm_I))) val_wb <= tagged Valid (1);
						else val_wb <= tagged Valid (0);
					  end
			f3_XORI  : val_wb <= tagged Valid (v1 ^ signExtend(imm_I));
			f3_ANDI  : val_wb <= tagged Valid (v1 & signExtend(imm_I));
			f3_ORI	 : val_wb <= tagged Valid (v1 | signExtend(imm_I));
			default  : no_match_1 = True;
		endcase

		case ({funct3, funct7})
			{f3_SLLI, f7_SLLI} : val_wb <= tagged Valid (v1 << shift_amt);
			{f3_SRLI, f7_SRLI} : val_wb <= tagged Valid (v1 >> shift_amt);
			{f3_SRAI, f7_SRAI} : val_wb <= tagged Valid (fn_right_arith_shift(v1, shift_amt));
			default : no_match_2 = True;
		endcase

		if (no_match_1 && no_match_2) wr_stop_reason <= CPU_STOP_INSTR_ERR;	// Instruction not supported

		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// Execute instructions performing LOAD
	rule rl_exec_load (opcode == op_LOAD && wr_instrn_valid);
		Bit #(32) imm_ext = signExtend(imm_I);
		LS_temp ls_temp = LS_temp {cmd: READ, data: ?, addr: v1 + signExtend(imm_I), bsize: ?, remark: ?};
		case (funct3)
			f3_LB   : begin ls_temp.bsize = sz_byte;  ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LH   : begin ls_temp.bsize = sz_hword; ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LW   : begin ls_temp.bsize = sz_word;  ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			//f3_LD : RV64 only
			f3_LBU  : begin ls_temp.bsize = sz_byte;  ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LHU  : begin ls_temp.bsize = sz_hword; ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LWU  : begin ls_temp.bsize = sz_word;  ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			default : wr_stop_reason <= CPU_STOP_INSTR_ERR;	// Instruction not supported
		endcase
		
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// Execute instructions performing LOAD
	rule rl_exec_store (opcode == op_STORE && wr_instrn_valid);
		LS_temp ls_temp = LS_temp {cmd: WRITE, data: ?, addr: v1 + signExtend(imm_S), bsize: ?, remark: ?};

		case (funct3)
			f3_SB   : begin ls_temp.bsize = sz_byte;  ls_temp.data = {24'b0, v2[7:0]};  val_ls <= tagged Valid ls_temp; end
			f3_SH   : begin ls_temp.bsize = sz_hword; ls_temp.data = {16'b0, v2[15:0]}; val_ls <= tagged Valid ls_temp; end
			f3_SW   : begin ls_temp.bsize = sz_word;  ls_temp.data = v2; 				val_ls <= tagged Valid ls_temp; end
			//f3_SD	: RV64 only
			default : wr_stop_reason <= CPU_STOP_INSTR_ERR; // Instruction not supported
		endcase
		
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// Execute control transfer instructions
	rule rl_exec_branch (opcode == op_BRANCH && wr_instrn_valid);
		GPR_Data_S sign_v1 = unpack(v1);
		GPR_Data_S sign_v2 = unpack(v2);

		GPR_Data_U usign_v1 = unpack(v1);
		GPR_Data_U usign_v2 = unpack(v2);
		
		case (funct3)
			f3_BEQ : begin
						if (v1 == v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (wr_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid}); 
					end

			f3_BNE : begin
						if (v1 != v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (wr_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid}); 
					end

			f3_BLT : begin
						if (sign_v1 < sign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (wr_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end
			
			f3_BGE : begin
						if (sign_v1 >= sign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (wr_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end

			f3_BLTU : begin
						if (usign_v1 < usign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (wr_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end

			f3_BGEU : begin
						if (usign_v1 >= usign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (wr_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end

			default : wr_stop_reason <= CPU_STOP_INSTR_ERR;		// Instruction not found
		endcase

		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule
	
	// Execute JALR instruction
	rule rl_exec_jalr ((opcode == op_JALR && funct3 == f3_JALR) && wr_instrn_valid);
		Addr target_addr = v1 + signExtend(imm_I);
		target_addr[0] = 0;
		Addr lr = wr_pc + zeroExtend(instlen_byte);
		val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid target_addr, rd: rd, lr: tagged Valid lr});
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// Execute JAL instruction
	rule rl_exec_jal (opcode == op_JAL && wr_instrn_valid);
		GPR_Data target_addr = wr_pc + signExtend(imm_J);
		Addr lr = wr_pc + zeroExtend(instlen_byte);
		val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid target_addr, rd: rd, lr: tagged Valid lr});
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// Execute LUI instruction
	rule rl_exec_lui (opcode == op_LUI && wr_instrn_valid);
		Data lui_imm = imm_U;
		val_wb <= tagged Valid lui_imm;
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// Execute AUIPC instruction
	rule rl_exec_auipc (opcode == op_AUIPC && wr_instrn_valid);
		Addr lui_imm = imm_U;
		lui_imm = wr_pc + lui_imm;
		val_wb <= tagged Valid lui_imm; 
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end
	endrule

	// =========== M Extension: MUL/DIV and Register-Register Instruction ============
	
	// =============== RV32M =========================================================

	// Function to perform 32-bit multiplication
	function GPR_Data fn_mul_result (Bit #(XPRLEN_2) op1, Bit #(XPRLEN_2) op2, Bool take_complement, Bit #(1) low_high);
		Bit #(XPRLEN_2) mul = op1 * op2;
		Integer xlen = valueOf(XPRLEN), xlen_2 = valueOf(XPRLEN_2);
		GPR_Data _result = ?;
		if (take_complement == True) mul = ~mul+1;
		case (low_high)
			0: _result = truncate(mul);
			1: _result = mul[xlen_2-1:xlen];
		endcase
	
		return _result;
	endfunction

	// Execute M Extension instructions. Register-Register instruction are also defined here as they have the same opcode
	rule rl_exec_mul_32 ( opcode == op_MUL_32 && wr_instrn_valid );
		Bit #(XPRLEN_2) op1 = ?;
		Bit #(XPRLEN_2) op2 = ?;
		Bool take_complement = False;
		Integer xlen = valueOf(XPRLEN), xlen_2 = valueOf(XPRLEN_2);
		Bit #(1) sn_v1 = v1[xlen-1], sn_v2 = v2[xlen-1];

		GPR_Data_S  sign_v1  = unpack(v1);
		GPR_Data_S  sign_v2  = unpack(v2);

		GPR_Data_U  usign_v1 = unpack(v1);
		GPR_Data_U  usign_v2 = unpack(v2);

		Bit #(5) shift_amt = v2[4:0];
		
		GPR_Data div_result = ?;

		case ({funct3, funct7})
			// MUL/DIV Instructions
			{f3_MUL,    f7_MUL}     : begin
							  	 		op1 = signExtend(v1);
								 		op2 = signExtend(v2);
								 		val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 0);
							   		  end
			{f3_MULH,   f7_MULH}    : begin
								  		op1 = extend((sn_v1 == 1)?(~v1+1):(v1));
								  		op2 = extend((sn_v2 == 1)?(~v2+1):(v2));
								  		take_complement = (sn_v1 == sn_v2)?False:True;
								  		val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 1);
			                	      end
			{f3_MULHSU, f7_MULHSU}  : begin
										op1 = extend((sn_v1 == 1)?(~v1+1):v1);
										op2 = extend(v2);
										take_complement = (sn_v1 == 1)?True:False;
										val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 1);
								   	  end
			{f3_MULHU,  f7_MULHU}   : begin
										op1 = extend(v1);
										op2 = extend(v2);
										val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 1);
				                      end

			{f3_DIV,    f7_DIV}     : if (sign_v2 == 0) val_wb <= tagged Valid -1;
									  else if (sign_v1 == xprlen_signed_low && sign_v2 == -1) val_wb <= tagged Valid pack(xprlen_signed_low);
									  else begin div_result = (pack)(sign_v1 / ((sign_v2==0)?1:sign_v2)); val_wb <= tagged Valid div_result; end

			{f3_DIVU,   f7_DIVU}    : if (usign_v2 == 0) val_wb <= tagged Valid pack(xprlen_unsigned_high);
									  else begin div_result = (pack)(usign_v1 / ((usign_v2==0)?1:usign_v2)); val_wb <= tagged Valid div_result; end

			{f3_REM,    f7_REM}     : if (sign_v2 == 0) val_wb <= tagged Valid pack(sign_v1);
									  else if (sign_v1 == xprlen_signed_low && sign_v2 == -1) val_wb <= tagged Valid 0;
									  else begin div_result = (pack)(sign_v1 % ((sign_v2==0)?1:sign_v2)); val_wb <= tagged Valid div_result; end

			{f3_REMU,   f7_REMU}    : if (usign_v2 == 0) val_wb <= tagged Valid pack(usign_v1);
									  else begin div_result = (pack)(usign_v1 % ((usign_v2==0)?1:usign_v2)); val_wb <= tagged Valid div_result; end
			
			// Register-Register Instructions
			{f3_ADD, f7_ADD}  		: val_wb <= tagged Valid (v1+v2);
			{f3_SUB, f7_SUB}  		: val_wb <= tagged Valid (v1-v2);
			{f3_SLL, f7_SLL}  		: begin val_wb <= tagged Valid (v1 << shift_amt); end
			{f3_SRL, f7_SRL}  		: val_wb <= tagged Valid (v1 >> shift_amt);
			{f3_SRA, f7_SRA}  		: val_wb <= tagged Valid (fn_right_arith_shift(v1, shift_amt));
			{f3_SLT, f7_SLT}  		: begin
										if (sign_v1 < sign_v2) val_wb <= tagged Valid (1);
										else val_wb <= tagged Valid (0);
					 				  end
	  		{f3_SLTU, f7_SLTU} 		: begin
										if (usign_v1 < usign_v2) val_wb <= tagged Valid (1);
										else val_wb <= tagged Valid (0);
							     	  end
			{f3_XOR, f7_XOR}  		: val_wb <= tagged Valid (v1 ^ v2);
			{f3_AND, f7_AND}  		: val_wb <= tagged Valid (v1 & v2);
			{f3_OR, f7_OR}    		: val_wb <= tagged Valid (v1 | v2);
			default   				: wr_stop_reason <= CPU_STOP_INSTR_ERR;
		endcase
		if (wr_verbosity > 2) begin $write($time, " CPU: EXECUTE: "); fn_report_exec_instr(); end

	endrule

	// =============== END: RV32M ====================================================

	// =============== END: M Extension: MUL/DIV =====================================

	// Prepare data to be committed
	rule rl_mkCommitData (wr_instrn_valid);
		Bool flag = False;
		CommitData cd = ?;

		// Following three cases are always mutually exclusive
		if (val_wb  matches tagged Valid .v)		begin cd = tagged WB fn_mk_wb (rd, v); end
		else if (val_wb_3 matches tagged Valid .v)	begin cd = tagged WB fn_mk_wb (rd, v); end
		else if (val_ls   matches tagged Valid .v)	begin cd = tagged LS fn_mk_ls (rd, v, 1); end
		else if (val_jmp  matches tagged Valid .v)	begin cd = tagged JMP  v; end
		else if (val_halt matches tagged Valid .v)  begin cd = tagged Halt v; end
		else begin flag = True; end

		if (!flag) wr_cd <= tagged Valid cd;
		else wr_cd <= tagged Invalid;
		
		if (wr_verbosity > 2)
			if (flag == False) $display($time," CPU: EXECUTE: Commit Data ready. PC: %h", wr_pc);
			else $display($time, " CPU: EXECUTE: Commit Data not available. Check CPU_STOP_REASON");
	endrule

	method Action write_executeData(IDEX_Data executeData);
		opcode <= executeData.opcode;
		rs1	   <= executeData.rs1;
		rs2	   <= executeData.rs2;
		rd	   <= executeData.rd;
		imm_I  <= executeData.imm_I;
		imm_S  <= executeData.imm_S;
		imm_B  <= executeData.imm_B;
		imm_U  <= executeData.imm_U;
		imm_J  <= executeData.imm_J;
		funct3 <= executeData.funct3;
		funct7 <= executeData.funct7;
		wr_pc  <= executeData.instrnAddr;
		wr_illegal_instruction <= executeData.illegal;
		wr_instrn_valid <= True;
	endmethod

	method GPR_Data read_gpr (RegName regname);
		return fn_read_gpr(regname);
	endmethod

	method Action write_gpr (RegName regd, GPR_Data data);
		regfile.upd(regd, data);
	endmethod

	method GPR_Data	read_exec_pc;
		return rg_last_exec_pc;
	endmethod

	method Action write_verbosity (int verbosity);
		wr_verbosity <= verbosity;
	endmethod

	method Wire #(Maybe #(CommitData)) read_cd;
		return wr_cd; 
	endmethod

	method Wire #(CPU_Stop_Reason) read_stop_reason;
		return wr_stop_reason;
	endmethod
	
endmodule

endpackage
