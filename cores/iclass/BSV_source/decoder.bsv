/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Author Names : Rahul Bodduna, N Sireesh
Email ID : rahul.bodduna@gmail.com
*/

package decoder;

import Vector:: *;
import riscv_types::*;
`include "defined_parameters.bsv"


(*noinline*)
function Decoded_info_type fn_decoder(Bit#(`INSTR_WIDTH) _instruction, Bit#(64) pc);
	Instruction_type lv_inst_type = USER_INT;
	Operation lv_op_name = NOP;
   	ALU_op lv_alu_op = ADD;
   	Mem_type lv_mem_type = NOP;
   	Mem_size lv_mem_size = _instruction[14:12];
   	Bool lv_imm_valid = False;
	Bool lv_csr_addr_valid = False;
   	Bit#(`REG_WIDTH) lv_imm = 0;
   	ALU_type lv_i_type = NOP;
   	Branch_type lv_branch_type = NOP;
   	Branch_op lv_branch_op = BEQ;
	MACHINE_op lv_csr_op = NOP;
   	Bit#(TLog#(`REGFILE_SIZE)) lv_rs1 = 0;
   	Bit#(TLog#(`REGFILE_SIZE)) lv_rs2 = 0;
   	Bit#(5) lv_rd = _instruction[11:7];
	Bool lv_rs1_valid = False;
	Bool lv_rs2_valid = False;
	Bool lv_rd_valid = True;
	Bool lv_word_flag = False;
	Exception_type lv_exception = No_exception;

	//Decoding the instruction, RS1, RS2, RD and imm/offset values
   	case (_instruction[6:2]) matches
	//LUI : TODO
	5'b01101: begin
		lv_imm_valid = True;
		lv_i_type = ALU;
		lv_alu_op = LUI;
		lv_imm = signExtend({_instruction[31:12],12'b0});
		lv_op_name = LUI;
	end

	//AUIPC : TODO
	5'b00101: begin
		lv_i_type = ALU;
		lv_alu_op = AUIPC;
		lv_imm_valid = True;
		Bit#(20) lv_pre_imm = _instruction[31:12];
		lv_imm = signExtend({lv_pre_imm,12'b0});
		lv_op_name = AUIPC;
	end
	
	//JAL
	5'b11011: begin
		lv_imm_valid = True;
		lv_imm = signExtend({_instruction[31],_instruction[19:12],_instruction[20],_instruction[30:21]});
		lv_branch_type = UNCOND;
		lv_branch_op = JAL;
	end

	//JALR
	5'b11001: begin
		lv_imm_valid = True;
		lv_imm = signExtend(_instruction[31:20]);
		lv_branch_type = UNCOND;
		lv_branch_op = JALR;
	end

	//Conditional branch instructions
	5'b11000: begin
		lv_imm_valid = True;
		lv_imm = signExtend({_instruction[31],_instruction[7],_instruction[30:25],_instruction[11:8]});
		lv_branch_type = COND;
		lv_rd_valid = False;
		lv_rd = 0;
		case (_instruction[14:12])
		//BEQ
		3'b000:	begin
			lv_branch_op = BEQ;
		end
		//BNE
		3'b001:	begin
			lv_branch_op = BNE;
		end
		//BLT
		3'b100:	begin
			lv_branch_op = BLT;
		end
		//BGE
		3'b101:	begin
			lv_branch_op = BGE;
		end
		//BLTU
		3'b110:	begin
			lv_branch_op = BLTU;
		end
		//BGEU
		3'b111:	begin
			lv_branch_op = BGEU;
		end
		//Illegal Instruction
		default : begin
			lv_exception = Illegal_instruction;
		end
			
		endcase
	end
	//Load instructions
	5'b00000: begin
		lv_imm_valid = True;
		lv_imm = signExtend(_instruction[31:20]);
		case (_instruction[14:12])
		3'b000:	begin
			lv_mem_type = LD;
		end
		3'b001:	begin
			lv_mem_type = LD;
		end
		3'b010:	begin
			lv_mem_type = LD;
		end
		3'b100:	begin
			lv_mem_type = LD;
		end
		3'b101:	begin
			lv_mem_type = LD;
		end
		3'b110: begin
			lv_mem_type = LD;
		end
		3'b011: begin
		 	lv_mem_type = LD;
		end
		//Illegal Instruction
		default : begin
			lv_exception = Illegal_instruction;
		end
		   
		endcase
	end
	//Store instructions
	5'b01000: begin
		lv_imm_valid = True;
		lv_imm = signExtend({_instruction[31:25],_instruction[11:7]});
		lv_rd_valid = False;
		lv_rd = 0;
		case (_instruction[14:12])
		3'b000:	begin
			lv_mem_type = STR;
		end
		3'b001:	begin
			lv_mem_type = STR;
		end
		3'b010:	begin
			lv_mem_type = STR;
		end
		3'b011: begin
			lv_mem_type = STR;
		end
		//Illegal Instruction
		default : begin
			lv_exception = Illegal_instruction;
		end
		endcase
	end
	//ALU instructions
	5'b0?1?0: begin
		if(_instruction[5] == 1 && _instruction[25] == 1) begin	
			if(_instruction[3] == 1'b1)
				lv_word_flag = True;

			case (_instruction[14:12])
			3'b000: begin
				lv_alu_op = MUL;
				lv_i_type = MUL;
			end
			3'b001: begin
				lv_alu_op = MULH;
				lv_i_type = MUL;
			end
			3'b010: begin
				lv_alu_op = MULHSU;
				lv_i_type = MUL;
			end
			3'b011: begin
				lv_alu_op = MULHU;
				lv_i_type = MUL;
			end
			3'b100: begin
				lv_alu_op = DIV;
				lv_i_type = DIV;
			end
			3'b101: begin
				lv_alu_op = DIVU;
				lv_i_type = DIV;
			end
			//Illegal Instruction
			default : begin
				lv_exception = Illegal_instruction;
			end
			endcase
		end
		else begin
			//In case of I type, imm is decoded
			if(_instruction[5] == 1'b0) begin
				lv_imm_valid = True;
				lv_imm = signExtend(_instruction[31:20]);
			end
			lv_i_type = ALU;
			if(_instruction[3] == 1'b1)
				lv_word_flag = True;

			case (_instruction[14:12])
			//ADDI,ADD,SUB
			3'b000:	begin
			if(_instruction[30] == 1 && _instruction[5] == 1)
				lv_alu_op = SUB;
			
			else lv_alu_op = ADD;
			end
			//SLT,SLTI
		 	3'b010:	begin
				lv_alu_op = SLT;
			end
			//SLTU,SLTUI
			3'b011:	begin
				lv_alu_op = SLTU;
			end
			//XOR,XORI
			3'b100:	begin
				lv_alu_op = XOR;
			end
			//OR,ORI
			3'b110:	begin
				lv_alu_op = OR;
			end
			//AND,ANDI
			3'b111:	begin
				lv_alu_op = AND;
			end
			//SLL,SLLI
			3'b001: begin
				lv_alu_op = SLL;
				if(_instruction[3] == 1'b1 && _instruction[25] == 1'b1) 
					lv_exception = Illegal_instruction;
			end
			//SRL,SRA,SRLI,SRAI
			3'b101: begin
				if(_instruction[3] == 1'b1 && _instruction[25] == 1'b1) 
					lv_exception = Illegal_instruction;
				case(_instruction[30])
				1'b0: begin
					lv_alu_op = SRL;
				end
				1'b1: begin
					lv_alu_op = SRA;
				end
				endcase
			end	
			//Illegal Instruction
			default : begin
				lv_exception = Illegal_instruction;
			end
			endcase
		end

	end
	5'b10011: begin
		if(_instruction[6] == 1'b1) begin
			lv_inst_type = SUPERVISOR;
			lv_imm	= zeroExtend(_instruction[31:20]);
			lv_csr_addr_valid = True;

			case(_instruction[13:12])
			2'b01: begin
				lv_csr_op = CSRRW;
			end
			2'b10: begin
				lv_csr_op = CSRRS;
			end
			2'b11: begin
				lv_csr_op = CSRRC;
			end
			endcase

			if(_instruction[14] == 1'b1) begin 	
				lv_rs1 = _instruction[19:15];
				lv_imm_valid = True;
			end
		end
		else 
			lv_exception = Illegal_instruction;
	end
	default : lv_exception = Illegal_instruction;
	endcase
	//Decoding value of RS1
	if(lv_branch_op != JAL || lv_op_name != LUI || lv_op_name != AUIPC || !(_instruction[5:0] == 6'b110011 && _instruction[14] == 1'b1)) begin
		lv_rs1_valid = True;
		lv_rs1 = _instruction[19:15];
	end
   
	//Decoding values of RS2
   	if(_instruction[6:2] == 5'b11000 || _instruction[6:2] == 5'b01000 
		|| _instruction[6:2] == 5'b01100 || _instruction[6:2] == 5'b01110) begin
		lv_rs2_valid = True;
		lv_rs2 = _instruction[24:20];
	end	

	//Returning the decoded instruction
   	return (Decoded_info_type {
	  inst_type: lv_inst_type,
	  mem_type:lv_mem_type,
	  mem_size:lv_mem_size,
	  alu_type:lv_i_type,
	  alu_op : lv_alu_op,
	  word_flag: lv_word_flag,
	  branch_type: lv_branch_type,
	  branch_op: lv_branch_op,
	  csr_inst_type: lv_csr_op,
	  imm_valid:lv_imm_valid,
	  csr_addr_valid : lv_csr_addr_valid,
	  rs1:lv_rs1,
	  rs1_valid: lv_rs1_valid,
	  rs2:lv_rs2,
	  rs2_valid: lv_rs2_valid,
	  rd:lv_rd,
	  rd_valid: lv_rd_valid,
	  imm:lv_imm,
	  exception: lv_exception 
	 });	
				
endfunction



endpackage 






                      
                   
