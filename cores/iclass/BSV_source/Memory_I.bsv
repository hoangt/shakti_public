package Memory_I;

import RegFile 		:: *;
import riscv_types 	:: *;
import All_types 	:: *;
import All_types_d 	:: *;
import TLM2 		:: *;
import Utils 		:: *;
import FIFOF		:: *;
import SpecialFIFOs	:: *;
import DefaultValue :: *;

`include "defined_parameters.bsv"
`define TLM_PRM_MEM_I_REQ 4, 64, 32, 8, Bit #(0)
`define TLM_PRM_MEM_I_RSP 4, 64, 32, 8, Bit #(0)

typedef TLMRequest #(`TLM_PRM_MEM_I_REQ) 		Req_Mem_I;
typedef RequestDescriptor #(`TLM_PRM_MEM_I_REQ) Req_Desc_Mem_I;
typedef RequestData #(`TLM_PRM_MEM_I_REQ) 	    Req_Data_Mem_I;

typedef TLMResponse #(`TLM_PRM_MEM_I_RSP) 		Rsp_Mem_I;


interface Memory_I_IFC;
	interface TLMRecvIFC #(Req_Mem_I, Rsp_Mem_I) bus_ifc;
endinterface

module mk_Memory_I #(String name) (Memory_I_IFC);

FIFOF #(Req_Mem_I) ff_reqs <- mkBypassFIFOF;
FIFOF #(Rsp_Mem_I) ff_rsps <- mkBypassFIFOF;

RegFile#(UInt#(`ICACHE_SIZE), Bit#(`INSTR_WIDTH)) memory_i <- mkRegFileLoad ("input.hex",0,`NUM_INSTRS);

rule rl_process_requests;
	
	Req_Mem_I request = ff_reqs.first; ff_reqs.deq;
    Bit#(`ADDRESS_WIDTH) icache_address = 0;
	Rsp_Mem_I rsp = defaultValue;

	if (request matches tagged Descriptor .r) begin
    	icache_address = r.addr >> 2;
		rsp.transaction_id = r.transaction_id;
		rsp.command = r.command;

		if (r.command == READ) begin
			rsp.data = memory_i.sub(unpack(icache_address));
			rsp.status = SUCCESS;
			$display($time, " %s: READ: Slave responding", name);
		end
		else if (r.command == WRITE) begin
			memory_i.upd(unpack(icache_address), r.data);
			rsp.status = SUCCESS;
			$display($time, " %s: WRITE: Slave Descriptor responding", name);
		end
		else begin
			rsp.status = ERROR;
		end
	end
	ff_rsps.enq (rsp);

endrule

interface bus_ifc = toRecvIFC (ff_reqs, ff_rsps);

endmodule

endpackage
