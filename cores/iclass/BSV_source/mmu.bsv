/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Author Names : Rahul Bodduna, Debiprasanna Sahoo.
Email ID : rahul.bodduna@gmail.com
*/


package mmu;

import ConfigReg::*;
import DReg::*;
import FIFOF::*;
import SpecialFIFOs::*;

import riscv_types::*;
import itlb_level1::*;
import dtlb_level1::*;
import tlb_level2::*;

`include "defined_parameters.bsv"

interface Ifc_mmu;
	/* Input methods */
	
	/* Input interface for the Instruction MMU to the CPU to get the virtual address, page identifier and data */	
	method Action _virtual_address_from_cpu_immu(Maybe#(Virtual_Address_Type) _virtual_address, Maybe#(Input_Data_Type) _data);

	/* Input interface for the Data MMU to the CPU to get the virtual address, page identifier and data */	
	method Action _virtual_address_from_cpu_dmmu(Maybe#(Virtual_Address_Type) _virtual_address, Maybe#(Input_Data_Type) _data);
	
	/* Input interface to the Page table to get the physical frame details */	
	method Action _physical_frame_details_from_page_table(Maybe#(Frame_details_type) _frame_details);
	
	/* Input interface to perform context switch action */
	method Action _perform_context_switch(Context_Switch_Type _context_switch);

	/* Input interface to the CPU to set the flush mode */	
	method Action _flush();

	/* Output Methods */
	
	/* Output Interface to the page table to set the virtual page number */
	method Maybe#(Virtual_Page_No_Type) _virtual_page_no_to_page_table();

	/* The page identifier details to the page table */
	method Page_Identifier _page_identifier_to_page_table();

	/* Notifies the page table walker/OS to get the physical frame  details from the page table */
	method Bool is_en_get_physical_frame();

	/* Notifies the CPU to send the next virtual address for Instruction MMU */
	method Bool is_en_get_next_virtual_address_immu();

	/* Notifies the CPU to send the next virtual address for Data MMU */
	method Bool is_en_get_next_virtual_address_dmmu();

endinterface

(*synthesize*)

/*
	Memory management module : It acts as an interface...	
*/
module mkmmu(Ifc_mmu);
	Ifc_itlb_level1						 itlb_level1	<-	mkitlb_level1;	// create an instance of L1 ITLB
	Ifc_dtlb_level1						 dtlb_level1	<-	mkdtlb_level1;	// create an instance of L1 DTLB
	Ifc_tlb_level2	 					  tlb_level2	<-	mktlb_level2;	// create an instance of L2 TLB
	
	ConfigReg#(Counter_Type)		  rg_count_page_table_access    <- 	mkConfigReg(0);// Count the number of MMU Page Access
	
	// MMU Register configurations
	Reg#(Logical_Partition_Identifier)   rg_logical_partition_identifier	<-	mkReg(0);	// Stores the current Guest OS ID which is requesting the TLB
	Reg#(Process_Identifier)		       rg_process_identifier	<-	mkReg(0);	// Stores the current process ID which is requesting the TLB
	Reg#(bit)					      rg_guest_state	<-	mkReg(0);	// Stores if guest or the hypervisor is using it
	Reg#(bit)					  rg_user_supervisor	<-	mkReg(0);	// Stores if the current access is performed by the user or the supervisor
	Reg#(Page_Size)						rg_page_size	<-	mkReg(0);	// Stores default page size of 4KB
	Reg#(bit)					       rg_tlb_select	<-	mkReg(0);	// Stores the TLB (Fixed Page Size or Variable Page Size) which is going to be selected
	
	Wire#(Maybe#(Frame_details_type))     wr_l2tlb_frame_details_from_pt	<-	mkDWire(tagged Invalid);	// Extended Details from the page table

	Reg#(Bool) 						    wr_flush	<-	mkDReg(False);	// Flush signal
	
	Reg#(Maybe#(Virtual_Page_No_Type)) rg_virtual_page_no_to_l2_tlb_immu	<-	mkReg(tagged Invalid);// Virtual Address from the CPU
	Reg#(Page_Identifier)		   rg_page_identifier_to_l2_tlb_immu	<- 	mkReg(	 	// Page identifier to the page table
	   Page_Identifier{_process_id:0,_logical_partition_id:0, _guest_state:0,_address_space:0,_user_supervisor:0});
	   
	Reg#(Maybe#(Virtual_Page_No_Type)) rg_virtual_page_no_to_l2_tlb_dmmu	<-	mkReg(tagged Invalid);// Virtual Address from the CPU
	Reg#(Page_Identifier)		   rg_page_identifier_to_l2_tlb_dmmu	<-	mkReg(	 	// Page identifier to the page table
	   Page_Identifier{_process_id:0,_logical_partition_id:0, _guest_state:0,_address_space:0,_user_supervisor:0});
	   
	FIFOF#(Frame_Output_Type)	              fifo_itlb_frame_output	<-	mkPipelineFIFOF;
	FIFOF#(Frame_Output_Type)	              fifo_dtlb_frame_output	<-	mkPipelineFIFOF;
	
	/* Incase of L1 ITLB and DTLB both requesting L2, this buffer stores the ITLB request giving priority to DTLB */
	Reg#(Maybe#(Translation_Request))	        rg_l2_request_buffer	<-	mkReg(tagged Invalid); 
	   
	Reg#(bit)					    rg_tlb_requester	<-	mkReg(0);	    // Stores if ITLB(0) or DTLB(1) made request to L2 TLB

	Reg#(Maybe#(Virtual_Page_No_Type))	    rg_virtual_page_no_to_pt	<-	mkReg(tagged Invalid);// Virtual Address to be sent to the page table l2tlb
	Reg#(Page_Identifier)		            rg_page_identifier_to_pt	<-	mkReg(	 	    // Page identifier to the page table
	   Page_Identifier{_process_id:0,_logical_partition_id:0, _guest_state:0,_address_space:0,_user_supervisor:0});
	
	ConfigReg#(Counter_Type)			            rg_clock	<-	mkConfigReg(0);	    // Clock counter

	ConfigReg#(Bool)		           rg_get_frame_from_l2_itlb	<-	mkConfigReg(False); // Register to keep track if request at ITLB is forwarded to L2 TLB
	ConfigReg#(Bool)		           rg_get_frame_from_l2_dtlb	<-	mkConfigReg(False); // Register to keep track if request at ITLB is forwarded to L2 TLB 	 	
	ConfigReg#(Bool) 		         rg_l2_tlb_get_frame_from_pt	<-	mkConfigReg(False); // Register to keep track if request at L2 TLB is forwarded to Page table

/********************************************************	RULES       ***************************************************************/

	/*
		Tracks the clock
	*/
	rule rl_count_clock;
		rg_clock 		<= 	rg_clock + 1;
	endrule

	/*
		Flush all content of the mmu
	*/
	rule  rl_flush(wr_flush);

		dtlb_level1._flush();
		itlb_level1._flush();
		tlb_level2._flush();
		fifo_itlb_frame_output.clear();
		fifo_dtlb_frame_output.clear();
	
		rg_virtual_page_no_to_l2_tlb_immu	<=	tagged Invalid;
		rg_virtual_page_no_to_l2_tlb_dmmu	<=	tagged Invalid;
		rg_virtual_page_no_to_pt 	 	<=	tagged Invalid;
		rg_l2_request_buffer		 	<=	tagged Invalid;

		rg_tlb_requester		 	<=	0;

		rg_page_identifier_to_l2_tlb_immu	<=     Page_Identifier{_process_id:0,_logical_partition_id:0, _guest_state:0,_address_space:0,_user_supervisor:0};
		rg_page_identifier_to_l2_tlb_dmmu	<=     Page_Identifier{_process_id:0,_logical_partition_id:0, _guest_state:0,_address_space:0,_user_supervisor:0};
		rg_page_identifier_to_pt 	 	<=     Page_Identifier{_process_id:0,_logical_partition_id:0, _guest_state:0,_address_space:0,_user_supervisor:0};

		rg_get_frame_from_l2_itlb	 	<=	False;
		rg_get_frame_from_l2_dtlb	 	<=	False;
		rg_l2_tlb_get_frame_from_pt	 	<=	False;

		rg_logical_partition_identifier		<=	0;
		rg_process_identifier			<=	0;
		rg_guest_state				<=	0;
		rg_user_supervisor			<=	0;
		rg_page_size				<=	0;
		rg_tlb_select				<=	0;		
		
	endrule
	


	
	/*
	 	This rule is fired when there is a miss in L2 TLB. The virtual address to page table register is set at the L2 TLB
	 	which notifies that there is a miss with this address at L2. This rule gets the address	the set the virtual address
	 	to page table register with the address from L2 TLB and enables the get physical address register for L2 TLB. This 
	 	will transfer control to the device that interfaces the MMU to get the physical address for the virtual address
	 	from the page table.
	 */
	rule rl_l2tlb_miss_request_by_immu(rg_virtual_page_no_to_l2_tlb_immu matches tagged Valid ._virtual_page_no 
					&&& rg_virtual_page_no_to_l2_tlb_dmmu matches tagged Invalid
					&&& rg_virtual_page_no_to_pt matches tagged Invalid
					&&& !rg_l2_tlb_get_frame_from_pt
					&&& tlb_level2._is_enable_get_frame_from_page_table()
					&&& !wr_flush);

		$display("Clock = %d\tStep  9:\tL2 TLB -> MMU\tMiss send virtual address = %h to page table",rg_clock,_virtual_page_no);

		// stores virtual address from the L2 TLB on which miss is generated
		rg_virtual_page_no_to_pt 	<=	tagged Valid _virtual_page_no;	
		// stores the page identifier from the L2 TLB which needs to be forwarded to the page table
		rg_page_identifier_to_pt	<=	rg_page_identifier_to_l2_tlb_immu; 			
		// Notifies the page table walker / OS to get the frame details from the page table
		rg_l2_tlb_get_frame_from_pt	<=	True;
	
	endrule

	/*
		This rule is fired when ITLB replies with the physical address with respect to the virtual address sent by the CPU.
		This response in buffered and then sent to the ICache.
	*/
	rule rl_output_physical_address_from_L1_ITLB(itlb_level1._physical_frame_to_l1_icache() matches tagged Valid ._physical_frame &&& !wr_flush);

		$display("Clock = %d\tStep 15:\tITLB -> ICache\tPhysical Address Generated = %h Write Back / Through = %h Write Allocate = %h Inhibiting = %h",
					rg_clock,{_physical_frame._physical_frame_no},
					_physical_frame._write_back_through,_physical_frame._write_allocate,_physical_frame._inhibiting);
		// Buffer the request in output instruction buffer
		fifo_itlb_frame_output.enq(_physical_frame);
	endrule

	/*
		Send the Buffered ITLB response to ICache when ICache is ready to take a request
	*/
	rule rl_send_from_L1_ITLB_to_ICache(!wr_flush && fifo_itlb_frame_output.notEmpty());
		fifo_itlb_frame_output.deq();
	endrule

	/*
	 	This rule is fired when there is a miss in L2 TLB. The virtual address to page table register is set at the L2 TLB
	 	which notifies that there is a miss with this address at L2. This rule gets the address	the set the virtual address
	 	to page table register with the address from L2 TLB and enables the get physical address register for L2 TLB. This 
	 	will transfer control to the device that interfaces the MMU to get the physical address for the virtual address
	 	from the page table.
	 */
	rule rl_l2tlb_miss_request_by_dmmu(rg_virtual_page_no_to_l2_tlb_dmmu matches tagged Valid ._virtual_page_no 
					&&& rg_virtual_page_no_to_l2_tlb_immu matches tagged Invalid
					&&& rg_virtual_page_no_to_pt matches tagged Invalid
					&&& !rg_l2_tlb_get_frame_from_pt
					&&& tlb_level2._is_enable_get_frame_from_page_table()
					&&& !wr_flush);

		$display("Clock = %d\tStep  9:\tL2 TLB -> MMU\tMiss send virtual address = %h to page table",rg_clock,_virtual_page_no);

		// stores virtual address from the L2 TLB on which miss is generated
		rg_virtual_page_no_to_pt 	<=	tagged Valid _virtual_page_no;	
		// stores the page identifier from the L2 TLB which needs to be forwarded to the page table
		rg_page_identifier_to_pt	<=	rg_page_identifier_to_l2_tlb_dmmu; 			
		// Notifies the page table walker / OS to get the frame details from the page table
		rg_l2_tlb_get_frame_from_pt	<=	True;
		// Increment page table access counter
		rg_count_page_table_access	<=	rg_count_page_table_access + 1;
	endrule

	/*
		This rule is fired when DTLB replies with the physical address with respect to the virtual address sent by the CPU.
		This response in buffered and then sent to the DCache.
	*/
	rule rl_output_physical_address_from_L1_DTLB(dtlb_level1._physical_frame_to_l1_dcache() matches tagged Valid ._physical_frame &&& !wr_flush);

		$display("Clock = %d\tStep 15:\tDTLB -> DCache\tPhysical Address Generated = %h Write Back / Through = %h Write Allocate = %h Inhibiting = %h",
					rg_clock,{_physical_frame._physical_frame_no},
					_physical_frame._write_back_through,_physical_frame._write_allocate,_physical_frame._inhibiting);
		// Buffer the request in output  data buffer
		fifo_dtlb_frame_output.enq(_physical_frame);
	endrule

	/*
		Send the Buffered DTLB response to DCache when DCache is ready to take a request
	*/
	rule rl_send_from_L1_DTLB_to_DCache(!wr_flush && fifo_dtlb_frame_output.notEmpty());
		fifo_dtlb_frame_output.deq();
	endrule






	/*
		When the L2 TLB free i.e. it can accept a new request, If there is a miss in L1 DTLB, the request is forwarded
		to L2 TLB. The virtual page numbers and page identifiers are also stored. If ITLB request, it is buffered in
		the L2 TLB request buffer to reserve race condition between ITLB and DTLB giving priority to DTLB.
	*/
	rule rl_request_l2tlb(rg_l2_request_buffer matches tagged Invalid &&& !wr_flush && !rg_get_frame_from_l2_dtlb && !rg_get_frame_from_l2_itlb
			&& tlb_level2._is_enable_get_next_request());

		// If there is miss in L1 DTLB and no L1 DTLB request is served by L2 TLB
		if(dtlb_level1._get_Translation_Request_for_l2_TLB() matches tagged Valid ._request  &&& !rg_get_frame_from_l2_dtlb)
		begin
			$display("Clock = %d\tStep  6:\tDTLB -> L2 TLB:\tSending Virtual Page Number = %h",rg_clock,_request._virtual_page_no);
			// send the address to the L2 TLB when miss in L1 DTLB
			tlb_level2._virtual_page_no_from_cpu(tagged Valid _request._virtual_page_no,
							_request._page_identifier,
							_request._readWrite);
			// Set that this L2 TLB request is made by L1 DTLB
			rg_tlb_requester			<=	1;
			// Store the virtual page number and page identifier for further use
			rg_virtual_page_no_to_l2_tlb_dmmu	<=	tagged Valid _request._virtual_page_no;
			rg_virtual_page_no_to_l2_tlb_immu	<=	tagged Invalid;
			rg_page_identifier_to_l2_tlb_dmmu	<=	_request._page_identifier;

			/*
				Set the register for DTLB which says that the request is forwarded from the L1 DTLB to L2 TLB.
				This is reset once the request is served by the L2 TLB or the Page table directly.
			 */
			rg_get_frame_from_l2_dtlb		<=	True;
		end
		// If there is miss in L1 ITLB and no L1 ITLB request is served by L2 TLB
		if(itlb_level1._get_Translation_Request_for_l2_TLB() matches tagged Valid ._request &&& !rg_get_frame_from_l2_itlb)
		begin
			$display("Clock = %d\tStep  6:\tITLB -> L2 TLB Buffer:\tSending Virtual Page Number = %h",rg_clock,_request._virtual_page_no);
			rg_l2_request_buffer		<=	tagged Valid _request;

			/*
				Set the register for ITLB which says that the request is forwarded from the L1 ITLB to L2 TLB.
				This is reset once the request is served by the L2 TLB or the Page table directly.
			*/		 
			rg_get_frame_from_l2_itlb		<=	True;
		end

	endrule

	/*
		The request stored in the L2 TLB buffer is forwarded to the L2 TLB when it is free and the virtual page numbers 
		and page identifiers are also stored
	*/
	rule rl_request_l2tlb_from_buffer(rg_l2_request_buffer matches tagged Valid ._request &&& !wr_flush
					&& tlb_level2._is_enable_get_next_request());

		$display("Clock = %d\tStep  6:\tL2 TLB Buffer -> L2 TLB:\tSending Virtual Address = %h\tRequester : 0",rg_clock,_request._virtual_page_no);
		// send the address to the L2 TLB when miss in L1 ITLB
		tlb_level2._virtual_page_no_from_cpu(tagged Valid _request._virtual_page_no,
						_request._page_identifier,
						0);
		// Set that this L2 TLB request is made by L1 ITLB
		rg_tlb_requester			<=	0;
		// Store the virtual page number and page identifier for further use
		rg_virtual_page_no_to_l2_tlb_immu	<=	tagged Valid _request._virtual_page_no;
		rg_virtual_page_no_to_l2_tlb_dmmu	<=	tagged Invalid;
		rg_page_identifier_to_l2_tlb_immu	<=	_request._page_identifier;
		// reset the L2 request buffer
		rg_l2_request_buffer			<=	tagged Invalid;
	endrule





	/*
		when the device interfacing with the MMU replies with physical address from the page table, the physical address
		is forwarded to the L2 TLB. It is simultenously forwarded to the L1 DTLB / ITLB on the basis of the "Address Space"
		bit in the page indentifier register.
	 */
	rule rl_forward_physical_frame_from_page_table_to_l2_tlb(rg_virtual_page_no_to_pt matches tagged Valid ._virtual_page_no_to_pt
				 &&& wr_l2tlb_frame_details_from_pt matches tagged Valid (._physical_frame_details)
				 &&& rg_l2_tlb_get_frame_from_pt && !wr_flush && (rg_get_frame_from_l2_itlb || rg_get_frame_from_l2_dtlb));
		
		$display("Clock = %d\tStep 11:\tPT -> L2 TLB:\tRecieved the physical frame  = %h",rg_clock,_physical_frame_details);

		// Send the physical frame from the page table to L2 TLB
		tlb_level2._physical_frame_from_page_table(tagged Valid _physical_frame_details);

		// when physical address is fetched from the page table this register is set to false to notify page table walker / OS
		rg_l2_tlb_get_frame_from_pt  		<= 	False;			  	 

		rg_virtual_page_no_to_pt		<=	tagged Invalid;

 		 // If the request is made by L1 DTLB forward it to DTLB
		if(rg_tlb_requester == `_address_space_data)
		begin
			$display("Clock = %d\tStep 12:\tPT -> DTLB:\tRecieved the physical address  = %h",rg_clock,_physical_frame_details);
			// Generate a frame for Data TLB from the frame response from Page Table
			DFrame_details_type _dframe	=	DFrame_details_type	{
				 _physical_frame_no:_physical_frame_details._physical_frame_no,
					_process_id:_physical_frame_details._process_id,
			      _logical_partition_id:_physical_frame_details._logical_partition_id,
				       _guest_state:_physical_frame_details._guest_state,
				_l1dtlb_access_ctrl:_physical_frame_details._l2tlb_access_ctrl,
				_write_back_through:_physical_frame_details._write_back_through,
				    _write_allocate:_physical_frame_details._write_allocate,
					_inhibiting:_physical_frame_details._inhibiting,
				_coherancy_required:_physical_frame_details._coherancy_required	};

			rg_get_frame_from_l2_dtlb	<=	False;
			// Update DTLB with the physical frame from the Page table directly
			dtlb_level1._physical_frame_from_l2_tlb(tagged Valid _dframe);
		end
		if(rg_tlb_requester == `_address_space_instruction)	// If the request is made by L1 ITLB forward it to ITLB
		begin
			$display("Clock = %d\tStep 12:\tPT -> ITLB:\tRecieved the physical address  = %h",rg_clock,_physical_frame_details);
			// Generate a frame for Instruction TLB from the frame response from Page Table
			IFrame_details_type _iframe	=	IFrame_details_type	{
				 _physical_frame_no:_physical_frame_details._physical_frame_no,
					_process_id:_physical_frame_details._process_id,
			      _logical_partition_id:_physical_frame_details._logical_partition_id,
				       _guest_state:_physical_frame_details._guest_state,
				_l1itlb_access_ctrl:_physical_frame_details._l2tlb_access_ctrl`_l1itlb_access_ctrl_index,	
				_write_back_through:_physical_frame_details._write_back_through,
				    _write_allocate:_physical_frame_details._write_allocate,
					_inhibiting:_physical_frame_details._inhibiting,
				_coherancy_required:_physical_frame_details._coherancy_required	};

			rg_get_frame_from_l2_itlb	<=	False;
			// Update ITLB with the physical frame from the Page table directly 			
			itlb_level1._physical_frame_from_l2_tlb(tagged Valid _iframe);
		end

	endrule
	
	/*
		when the L2 TLB gets the physical frame it forwards the same to the respective L1 TLB.It forwards the frame details
		to the requesting TLB. The requesting TLB type can be found in the frame details itself which has been forwarded by
		the L2 TLB. After receiving frame details from L2 TLB the respective L1 DTLB and ITLB take the response and update 
		their array.
	 */
	rule rl_forward_physical_frame_from_l2_tlb_to_l1_tlb(rg_virtual_page_no_to_pt matches tagged Invalid
			&&& tlb_level2._physical_frame_details_to_l1_tlb_cache() matches tagged Valid (._physical_frame_details) 
			&&& !wr_flush && (rg_get_frame_from_l2_itlb || rg_get_frame_from_l2_dtlb));

		Frame_details_type	_frame 	 = _physical_frame_details;
		tlb_level2._response_forwarded_to_l1_tlb();
		// If the request is made by L1 DTLB forward it to DTLB
		if(_frame._l1tlb_type == `_address_space_data && rg_get_frame_from_l2_dtlb)
		begin
			$display("Clock = %d\tStep 14:\tL2 TLB -> DTLB:\tRecieved the physical address  = %h",rg_clock,_physical_frame_details);
			// Generate a frame for Data TLB from the frame response from L2 TLB
			DFrame_details_type _dframe	=	DFrame_details_type	{
				 _physical_frame_no:_frame._physical_frame_no,
					_process_id:_frame._process_id,
			      _logical_partition_id:_frame._logical_partition_id,
				       _guest_state:_frame._guest_state,
				_l1dtlb_access_ctrl:_frame._l2tlb_access_ctrl,	
				_write_back_through:_frame._write_back_through,
				    _write_allocate:_frame._write_allocate,
					_inhibiting:_frame._inhibiting,
				_coherancy_required:_frame._coherancy_required	};
	
			rg_get_frame_from_l2_dtlb	<=	False;
			// Update DTLB with the physical frame from the L2 TLB
			dtlb_level1._physical_frame_from_l2_tlb(tagged Valid _dframe);
		end
		else if(rg_get_frame_from_l2_itlb)	// If the request is made by L1 ITLB forward it to ITLB
		begin
			$display("Clock = %d\tStep 14:\tL2 TLB -> ITLB:\tRecieved the physical address  = %h",rg_clock,_physical_frame_details);
			// Generate a frame for Instruction TLB from the frame response from L2 TLB
			IFrame_details_type _iframe	=	IFrame_details_type	{
				 _physical_frame_no:_frame._physical_frame_no,
					_process_id:_frame._process_id,
			      _logical_partition_id:_frame._logical_partition_id,
				       _guest_state:_frame._guest_state,
				_l1itlb_access_ctrl:_frame._l2tlb_access_ctrl`_l1itlb_access_ctrl_index,
				_write_back_through:_frame._write_back_through,
				    _write_allocate:_frame._write_allocate,
					_inhibiting:_frame._inhibiting,
				_coherancy_required:_frame._coherancy_required	};

			rg_get_frame_from_l2_itlb	<=	False;
			// Update ITLB with the physical frame from the L2 TLB 			 			
			itlb_level1._physical_frame_from_l2_tlb(tagged Valid _iframe);
		end
	endrule

	rule rl_display(rg_clock == 100);
		$display("L2 TLB : Access\t\t=\t%d",tlb_level2._access_count());		
		$display("L2 TLB : Hits\t\t=\t%d",tlb_level2._hit_miss_count()`_counter_LSBs);
		$display("L2 TLB : Misses\t\t=\t%d",tlb_level2._hit_miss_count()`_counter_MSBs);		

		$display("L1 ITLB : Access\t\t=\t%d",itlb_level1._access_count());		
		$display("L1 ITLB : Hits\t\t=\t%d",itlb_level1._hit_miss_count()`_counter_LSBs);
		$display("L1 ITLB : Misses\t\t=\t%d",itlb_level1._hit_miss_count()`_counter_MSBs);		
		
		$display("L1 DTLB : Access\t\t=\t%d",dtlb_level1._access_count());
		$display("L1 DTLB : Hits\t\t=\t%d",dtlb_level1._hit_miss_count()`_counter_LSBs);
		$display("L1 DTLB : Misses\t\t=\t%d",dtlb_level1._hit_miss_count()`_counter_MSBs);		
		$finish(0);
	endrule
	
/************************************************************************************************************************************************/
	/* Input Methods*/
	
	/* Input interface to the CPU to get the extended virtual address and related information */
	method Action _virtual_address_from_cpu_immu(Maybe#(Virtual_Address_Type) _virtual_address, Maybe#(Input_Data_Type) _data);
		if(_virtual_address matches tagged Valid .address)
			itlb_level1._virtual_page_no_from_cpu(tagged Valid address`_virtual_page_no,Page_Identifier	{
								_process_id	:	rg_process_identifier,
						      _logical_partition_id	:	rg_logical_partition_identifier,
							       _guest_state	:	rg_guest_state,
							     _address_space	:	0,
							   _user_supervisor	:	rg_user_supervisor});

	endmethod
	
	/* Input interface to the CPU to get the extended virtual address and related information */
	method Action _virtual_address_from_cpu_dmmu(Maybe#(Virtual_Address_Type) _virtual_address, Maybe#(Input_Data_Type) _data);
		if(_virtual_address matches tagged Valid .address)
			if(_data matches tagged Valid .x)
				dtlb_level1._virtual_page_no_from_cpu(tagged Valid address`_virtual_page_no,Page_Identifier	{
								_process_id	:	rg_process_identifier,
						      _logical_partition_id	:	rg_logical_partition_identifier,
							       _guest_state	:	rg_guest_state,
							     _address_space	:	1,
							   _user_supervisor	:	rg_user_supervisor},1);
			else
				dtlb_level1._virtual_page_no_from_cpu(tagged Valid address`_virtual_page_no,Page_Identifier	{
								_process_id	:	rg_process_identifier,
						      _logical_partition_id	:	rg_logical_partition_identifier,
							       _guest_state	:	rg_guest_state,
							     _address_space	:	1,
							   _user_supervisor	:	rg_user_supervisor},0);
	endmethod
	
	/* Input interface to the Page table to get the extended physical frame number */
	method Action _physical_frame_details_from_page_table(Maybe#(Frame_details_type) _frame_details);
		wr_l2tlb_frame_details_from_pt	<= 	_frame_details;				// get the partition and process details from the Page table for the IMMU
	endmethod

	/* Input interface to the CPU to set the flush mode */
	method Action _flush();
		wr_flush	 		<= 	True;
	endmethod

	/* Input interface to perform context switch action */
	method Action _perform_context_switch(Context_Switch_Type _context_switch) if(!wr_flush);
		rg_logical_partition_identifier	<=	_context_switch._logical_partition;	// Stores the current Guest OS ID which is requesting the TLB
		rg_process_identifier		<=	_context_switch._process_identifier;	// Stores the current process ID which is requesting the TLB
		rg_guest_state			<=	_context_switch._guest_state;		// Stores if guest or the hypervisor is using it
		rg_user_supervisor		<=	_context_switch._user_supervisor;	// Stores if the current access is performed by the user or the supervisor
		rg_page_size			<=	_context_switch._page_size;		// Stores the page size
		rg_tlb_select			<=	_context_switch._tlb_select;		// Stores the TLB (fixed size or variable size) which is needs to be requested
	endmethod

	/* Output methods*/

	/* 
		virtual page to page table to check for physical page in case of a miss 
	*/	
	method Maybe#(Virtual_Page_No_Type) _virtual_page_no_to_page_table();
		return rg_virtual_page_no_to_pt;						// Send the virtual page to the page table
	endmethod

	/*
		The page identifier details to the page table
	*/
	method Page_Identifier _page_identifier_to_page_table();
		return rg_page_identifier_to_pt;						// Send the page indentifier to the page table
	endmethod

	/*
		When it has vacant sapce in DMMU Buffer,it notifies the CPU to send the next address for the Data MMU.	
	*/
	method Bool is_en_get_next_virtual_address_dmmu();
		return 	dtlb_level1._is_enable_get_page_from_mmu();
	endmethod
	
	/*
		When it has vacant space in IMMU Buffer,it notifies the CPU to send the next address for the Instruction MMU.	
	*/
	method Bool is_en_get_next_virtual_address_immu();
		return itlb_level1._is_enable_get_page_from_mmu();
	endmethod

	/*
		It enables the hardware interfacing for the page table to get the physical frame from the page table
	*/
	method Bool is_en_get_physical_frame();
		return rg_l2_tlb_get_frame_from_pt;
	endmethod

endmodule
endpackage
