package Tb_Memory_I;

import RegFile 		:: *;
import riscv_types 	:: *;
import All_types 	:: *;
import All_types_d 	:: *;
import TLM2 		:: *;
import Utils 		:: *;
import FIFOF		:: *;
import SpecialFIFOs	:: *;
import DefaultValue :: *;
import Connectable	:: *;
import Memory_I		:: *;
import Axi			:: *;
import Vector		:: *;

`include "defined_parameters.bsv"
`include "TLM.defines"
`include "Axi.defines"

`define TLM_PRM_MEM_I_REQ 4, 64, 32, 8, Bit #(0)
`define TLM_PRM_MEM_I_RSP 4, 64, 32, 8, Bit #(0)
`define AXI_PRM_MEM_I     4, 64, 32, 8, Bit #(0)
`define AXI_XTR_MEM_I TLMRequest #(`TLM_PRM_MEM_I_REQ), TLMResponse #(`TLM_PRM_MEM_I_RSP), `AXI_PRM_MEM_I

typedef TLMRequest #(`TLM_PRM_MEM_I_REQ) 		   Req_Mem_I;
typedef RequestDescriptor #(`TLM_PRM_MEM_I_REQ)	   Req_Desc_Mem_I;
typedef RequestData #(`TLM_PRM_MEM_I_REQ) 	       Req_Data_Mem_I;

typedef TLMResponse #(`TLM_PRM_MEM_I_RSP) 		   Rsp_Mem_I;

interface Master_I_IFC;
	interface TLMSendIFC #(Req_Mem_I, Rsp_Mem_I) mem_i_rd_ifc;
	interface TLMSendIFC #(Req_Mem_I, Rsp_Mem_I) mem_i_wr_ifc;

method Action put_read_request (Req_Mem_I req);
	method ActionValue #(Rsp_Mem_I) get_read_response;
//	method Action put_write_request (Req_Mem_I req);
//	method Rsp_Mem_I get_write_response;
endinterface

module mkTb_Memory_I (Empty);

	Reg #(int) rg_cnt <- mkReg(0);
	
	//Memory_I_IFC mem_i    <- mk_Memory_I;
	//Master_I_IFC source_0 <- mkMaster_I;
   
   function Bool addrMatch0(AxiAddr#(`TLM_TYPES) a);
      return (a[4] == 0);
   endfunction
   
   function Bool addrMatch1(AxiAddr#(`TLM_TYPES) a);
      return (a[4] == 1);
   endfunction

   // Read Master Transactor
   AxiRdMasterXActorIFC#(`AXI_XTR_MEM_I) rd_master_0 <- mkAxiRdMaster;
   AxiRdMasterXActorIFC#(`AXI_XTR_MEM_I) rd_master_1 <- mkAxiRdMaster;

   // Write Master Transactor
   AxiWrMasterXActorIFC#(`AXI_XTR_MEM_I) wr_master_0 <- mkAxiWrMaster;
   AxiWrMasterXActorIFC#(`AXI_XTR_MEM_I) wr_master_1 <- mkAxiWrMaster;
   
   // Read Slave Transactor
   AxiRdSlaveXActorIFC#(`AXI_XTR_MEM_I)  rd_slave_0 <- mkAxiRdSlave(1, addrMatch0);
   AxiRdSlaveXActorIFC#(`AXI_XTR_MEM_I)  rd_slave_1 <- mkAxiRdSlave(1, addrMatch1);
   
   // Write Slave Transactor
   AxiWrSlaveXActorIFC#(`AXI_XTR_MEM_I)  wr_slave_0 <- mkAxiWrSlave(1, addrMatch0);
   AxiWrSlaveXActorIFC#(`AXI_XTR_MEM_I)  wr_slave_1 <- mkAxiWrSlave(1, addrMatch1);
   
   // Read Write Master Instantiation
   Master_I_IFC source_0 <- mkMaster_I ("Master_0");
   Master_I_IFC source_1 <- mkMaster_I ("Master_1");

   // Read Slave Instantiation
   Memory_I_IFC  rd_mem_0   <- mk_Memory_I ("Rd_Slave_0");
   Memory_I_IFC  rd_mem_1   <- mk_Memory_I ("Rd_Slave_1");

   // Write Slave Instantiation
   Memory_I_IFC  wr_mem_0   <- mk_Memory_I ("Wr_Slave_0");
   Memory_I_IFC  wr_mem_1   <- mk_Memory_I ("Wr_Slave_1");
 
   // Connections between Read masters and transactor's tlm interface
   mkConnection(source_0.mem_i_rd_ifc, rd_master_0.tlm);
   mkConnection(source_1.mem_i_rd_ifc, rd_master_1.tlm);

   // Connections between Write masters and transactor's tlm interface
   mkConnection(source_0.mem_i_wr_ifc, wr_master_0.tlm);
   mkConnection(source_1.mem_i_wr_ifc, wr_master_1.tlm);

   // Instantiating read bus fabric
   Vector #(2, AxiRdFabricMaster#(`AXI_PRM_MEM_I)) rd_masters = newVector;
   Vector #(2, AxiRdFabricSlave#(`AXI_PRM_MEM_I))  rd_slaves  = newVector;
   
   // Instantiating write bus fabric
   Vector #(2, AxiWrFabricMaster#(`AXI_PRM_MEM_I)) wr_masters = newVector;
   Vector #(2, AxiWrFabricSlave#(`AXI_PRM_MEM_I))  wr_slaves  = newVector;

   // Linking read sources with bus fabric through read bus transactors
   rd_masters[0] = rd_master_0.fabric;
   rd_masters[1] = rd_master_1.fabric;

   // Linking read targets with bus fabric through read bus transactors
   rd_slaves [0] = rd_slave_0.fabric;
   rd_slaves [1] = rd_slave_1.fabric;

   // Linking write sources with bus fabric through write bus transactors
   wr_masters[0] = wr_master_0.fabric;
   wr_masters[1] = wr_master_1.fabric;
   
   // Linking write targets with bus fabric through write bus transactors
   wr_slaves [0] = wr_slave_0.fabric;
   wr_slaves [1] = wr_slave_1.fabric;
   
   // Instantiating read bus (channel)
   mkAxiRdBus(rd_masters, rd_slaves);

   // Instantiating write bus (channel)
   mkAxiWrBus(wr_masters, wr_slaves);
   
   // Connections between read slaves and transactor's tlm interface
   mkConnection(rd_slave_0.tlm, rd_mem_0.bus_ifc);
   mkConnection(rd_slave_1.tlm, rd_mem_1.bus_ifc);

   // Connections between write slaves and transactor's tlm interface
   mkConnection(wr_slave_0.tlm, wr_mem_0.bus_ifc);
   mkConnection(wr_slave_1.tlm, wr_mem_1.bus_ifc);
	
   // -- mine -- mkConnection (master_i.mem_i_ifc, mem_i.bus_ifc);

	rule rl_mk_rd_request;
		Req_Desc_Mem_I request = defaultValue;
		request.command = READ;
		request.addr	= ("Master_0" == "Master_0") ? 64'h0004:64'h014;
		request.transaction_id = 1;
		request.burst_size = 2'b11;
		request.burst_length = 1;
		Req_Mem_I req = tagged Descriptor request;
		source_0.put_read_request(req);
	endrule

	rule rl_get_rd_response;
		let rsp <-  source_0.get_read_response;
		//$display ($time, " READ: Recieved data: %h", rsp.data);
	endrule

	rule rl_cntr;
			$display($time);
		if (rg_cnt == 20) $finish;
		else rg_cnt <= rg_cnt + 1;
	endrule

endmodule

module mkMaster_I #(String name) (Master_I_IFC);

	FIFOF #(Req_Mem_I) ff_reqs_rd <- mkBypassFIFOF;
	FIFOF #(Rsp_Mem_I) ff_rsps_rd <- mkBypassFIFOF;

	FIFOF #(Req_Mem_I) ff_reqs_wr <- mkBypassFIFOF;
	FIFOF #(Rsp_Mem_I) ff_rsps_wr <- mkBypassFIFOF;

	interface mem_i_rd_ifc = toSendIFC (ff_reqs_rd, ff_rsps_rd);
	interface mem_i_wr_ifc = toSendIFC (ff_reqs_wr, ff_rsps_wr);

	method Action put_read_request (Req_Mem_I req);
		ff_reqs_rd.enq(req);
		$display ($time, " %s: READ: Putting request to Address %h", name, req.Descriptor.addr);
	endmethod

	method ActionValue #(Rsp_Mem_I) get_read_response;
		Rsp_Mem_I rsp = ff_rsps_rd.first;
		ff_rsps_rd.deq;
		$display ($time, " %s: READ: Recieved data: %h", name, rsp.data);
		return rsp;
	endmethod

endmodule

endpackage
